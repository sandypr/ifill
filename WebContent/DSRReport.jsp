<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ifill : Stock Receipt</title>
<link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
<style type="text/css">
.Text_grey {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #999999;
}
</style>
<script type="text/javascript">
	$(document).ready(function(){
	
		var rowList = {};
		$.ajax({
				type : "GET",
				url : "/ifill/ifill/reports/dsrData",
				contentType : 'application/json',
				dataType : "text",
				success : function(rows) {												
					var finalData = jQuery.parseJSON(rows);
					rowList = finalData.reportRespList;
					paintGrid();
					
				},
				error : function(xhr) {
					alert("Opps !! We could not delete the row now. Please try again. Please find the cause : "
							+ xhr.status);
				}
			});
			
		function paintGrid(){
			
			$('#balanceData').append("<tr id='row0'> </tr>");

			var cumulativeMeterSales = 0;
			var cumulativeVariation = 0;
			$.each(rowList, function(index, row) {
				var dipSales = 0;
				var dailyVariation = 0;
				
				console.log(row);
				cumulativeMeterSales =+ Number(row['saleLtr']);
				
				var nextDip = rowList[index+1]['dip'];
				dipSales = Number(row['dip']) + Number(nextDip);
				
				dailyVariation = Number(row['saleLtr']) - Number(dipSales);
				cumulativeVariation =+  dailyVariation;
				

				$('#row'+index).append("<td align='right'><div class='col-md-12'>"+row['date']+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+row['dip']+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+row['reciept']+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+row['totalStock']+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+row['closing']+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+row['test']+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+row['saleLtr']+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+cumulativeMeterSales+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+dipSales+"</div></td>");
				$('#row'+index).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+cumulativeVariation+"</div></td>");
				
				$('#balanceData').append("<tr id='row"+(index+1)+"'> </tr>");
			});
			
		}
		
	});
</script>
</head>
<body>

<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="604" height="30"><span class="logo">I-</span><span
				class="logo1">fill </span></td>
			<td width="399" height="45" align="right" valign="top"><a href="#" id='logoutButton'><img src="${pageContext.request.contextPath}/images/logout.png" alt="logout.png" width="120" height="40" /></a></td>
		</tr>
	</table>
	
	<br />
<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="252" height="40" align="left" valign="middle" class="subheading" style="background-image:url(${pageContext.request.contextPath}/images/report_button.png); background-repeat:no-repeat">&nbsp;Monthly DSR</td>
    <td width="751"><br />
   </td>
  </tr>
</table>

<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="887">
    <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #cccccc;
    border-radius:5px; background-image:url(${pageContext.request.contextPath}/images/login_bg.jpg); background-repeat:repeat" bgcolor="#f7f7f7">
      <tr>
        <td height="42" align="center" valign="middle"></br>
          <table>
				<tr align="center"><td><h3>ABC Filling Station, </br></h3><h4>PQR Road, XYZ Dist.</h4></td></tr>
				<!-- <tr align="center"><td><h4>Tanikella Road, Khammam Dist.</h4></td></tr> -->							
			</table> 
			<table width="980" border="0" align="center" cellpadding="0"
				cellspacing="0">
				<tr align="left">
					<td width="418"></td>
					<td width="562" height="25" align="center" valign="bottom">
							&nbsp;&nbsp;<span class="para5">Date : <u><%-- <%= session.getAttribute("Date").toString(); %> --%></u></span>
						</td>
				</tr>
			</table><hr width="75%" align="center" /></br>                
            <div class="col-md-offset-1 col-md-10 column">
            	<table class='table table-condensed table-striped table-bordered' id='balanceData'>
            			<tr>
            				<th>Date</th>
            				<th>Opening Dip</th>
            				<th>Stock Reciept</th>
            				<th>Total Stock</th>
            				<th>Meter Sales</th>
            				<th>Pump Test</th>
            				<th>Net Meter Sales </th>
            				<th>Cumulative Sales</th>
            				<th>Dip Sales</th>
            				<th>Daily Variation</th>
            				<th>Cumulative Variation</th>
            			</tr>
            	</table>
            </div>
				
		<br />
		<div class="col-md-offset-1 col-md-10 column" id="test">
							<table ><tr><td align="left"></td>
								<td align="right"></td>
								<td  class="col-md-offset-1 col-md-10" ></td>
								<td  class="col-md-offset-1 col-md-10" ><a id='saveButton' href="#" class="pull-left btn btn-success" onclick="divPrint();">Print / Save</a></td>
								<td><a id='backbutton' href="/ifill/ifill/inventory/showHomePage" class="pull-left btn btn-warning">Go Back</a></td>
								</table>
								<script type="text/javascript">
							      function divPrint() {
							      	//alert("printing");
							        //$("#print_div").addClass("printable");
							        //$('body').css('display','none');
							        
							        //$('#print_div').css('display', 'block');
							        //alert('added printable');
							        $('#logout_button').css('display','none');
							        $('#saveButton').css('display','none');
							        $('#backbutton').css('display','none');
							        window.print();
							      }
							    </script>
						</div>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<br />
<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" align="center" bgcolor="#8780b0" class="copyright">@copyright 2013 . All Rights Reserved</td>
  </tr>
</table>
</body>
</html>