<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ifill : Initial Readings</title><link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
<style type="text/css">
.Text_grey {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #999999;
}
</style>
<script type="text/javascript">

	$(document)
			.ready(
					function() {
					
						$.ajax({
							type : "GET",
							url : "/ifill/ifill/metadata/fuels",
							contentType : 'application/json',
							dataType : "text",
							success : function(ldr) {
								var finalData = jQuery.parseJSON(ldr);
								$.each(finalData, function(index, value) {
								//alert(value);
								$("#productType").append(
										'<option value="'+value+'">' + value
												+ '</option>');
								
								});
							},
							error : function(xhr) {
								//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
								$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
							
							}
						});
						
						
						(function($) {
								$.fn.serializeObject = function() {
									var self = this, json = {}, push_counters = {}, patterns = {
										"validate" : /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
										"key" : /[a-zA-Z0-9_]+|(?=\[\])/g,
										"push" : /^$/,
										"fixed" : /^\d+$/,
										"named" : /^[a-zA-Z0-9_]+$/
									};
									this.build = function(base, key, value) {
										base[key] = value;
										return base;
									};
									this.push_counter = function(key) {
										if (push_counters[key] === undefined) {
											push_counters[key] = 0;
										}
										return push_counters[key]++;
									};
						$.each($(this).serializeArray(),
							function() {
								// skip invalid keys
								if (!patterns.validate.test(this.name)) {
									return;
								}
								var k, keys = this.name.match(patterns.key), merge = this.value, reverse_key = this.name;
								while ((k = keys.pop()) !== undefined) {
									// adjust reverse_key
									reverse_key = reverse_key.replace(
											new RegExp("\\[" + k + "\\]$"), '');
									// push
									if (k.match(patterns.push)) {
										merge = self.build([], self
												.push_counter(reverse_key),
												merge);
									}
									// fixed
									else if (k.match(patterns.fixed)) {
										merge = self.build([], k, merge);
									}
									// named
									else if (k.match(patterns.named)) {
										merge = self.build({}, k, merge);
									}
								}
								json = $.extend(true, json, merge);
							});
			return json;
		};
	})(jQuery);
	
					$('#productType').change(function(e){
						//alert("changed the product");
						var product = $('#productType').val();
						if(product != 'XXX'){
							
							$.ajax({
								type : "GET",
								url : "/ifill/ifill/metadata/"+product+"/pumps",
								contentType : 'application/json',
								dataType : "text",
								success : function(ldr) {
									$("#pumpId").html("");
									$("#pumpId").append(
											"<option value='XXX'>Select&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </option>");
									var finalData = jQuery.parseJSON(ldr);
									$.each(finalData, function(index, value) {
									console.log(value);
									$("#pumpId").append(
											'<option value="'+value.pump_id+'">' + value.name
													+ '</option>');
									
									});
								},
								error : function(xhr) {
									//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
									$("#errorModal").modal('show');
									$("#errorMsg").html("");
									$("#errorMsg").append("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
								
								}
							});
						}
						
					});
	
					// validations
					$('#pumpId').change(function(e){
						var val = $('#pumpId option:selected').val();
						if(val == "XXX"){
							//alert('Please select a Pump to proceed.');
							$("#errorModal").modal('show');
							$("#errorMsg").html("");
							$("#errorMsg").append('Please select a Pump to proceed.');
						}else{
								$('#quantity').val("");
								$('#pumpTest').val("");
								$('#ldsale').val("");
								$('#ldsalers').val("");
								$('#dayDate').val("");
						}
					});
					
					$('#quantity').blur(function(e){
						var val = $('#quantity').val();
						if(val.length < 1 || !$.isNumeric(val)){
							//alert('Please provide a valid number for Reading.');
							$("#errorModal").modal('show');
							$("#errorMsg").html("");
							$("#errorMsg").append('Please provide a valid number for Reading.');
						
						}
					});
					
					
					// Submision of values
					var readings = 1;		
					var KEY_SEPERATOR = "~";			
					$('#submit_row').click(function(e){
						var pump = $('#pumpId').val();
						var quantity = $('#quantity').val();
						var pumpTest = $('#pumpTest').val();
						var date = $('#dayDate').val();
						e.preventDefault();
						if(pump == 'XXX' || quantity.length < 1 || pumpTest.length < 1 || date.length < 1){
							//alert('Please provide values for all the fields before submitting.');
							$("#errorModal").modal('show');
							$("#errorMsg").html("");
							$("#errorMsg").append('Please provide proper values to all fields before submitting.');
						
						}else {
							console.log('submiting');
							console.log($('#dailyReadings').attr('action'));
							$.ajax({
								type : "POST",
								url : $('#dailyReadings').attr('action'),
								data : JSON.stringify($('#dailyReadings').serializeObject()),
								contentType : 'application/json',
								dataType : "text",
								success : function(loggedIn) {
									
									var ldsale = Number(quantity)-Number(pumpTest);
									
									var key = "readingsGrid"+readings+ KEY_SEPERATOR +pump+ KEY_SEPERATOR +date;
									$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'><a href='#' id="+key+" name="+key+"  onClick='deleteRow(this)'><img src='${pageContext.request.contextPath}/images/delete_btn.png' alt='delete_btn.png' width='15' height='15'/> </a></div></td>");
						
									$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+pump+"</div></td>");									
									$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+date+"</div></td>");
									$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+quantity+"</div></td>");
									$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+pumpTest+"</div></td>");
									$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+ldsale+"</div></td>");
									
									readings = readings + 1;
									$('#tab_logic').append("<tr id='readingsGrid"+readings+"' ></tr>");
									
									// clear values
									$('#productType').val("XXX");
									$('#pumpId').val("XXX");
									$('#quantity').val("");
									$('#pumpTest').val("");
									$('#dayDate').val("");
								},
								error : function(xhr) {
									$("#errorModal").modal('show');
									$("#errorMsg").html("");
									$("#errorMsg").append("Opps !! Something went wrong at server. <br>Please contact admin for further help : " + xhr.status);
								}
							});
							return false;
						}
		
		$('#logoutButton').click(function(e){
			e.preventDefault();
			$.ajax({
				type : "GET",
				url : "/ifill/ifill/profile/logout",
				contentType : 'application/json',
				dataType : "text",
				success : function(status) {												
					//alert('successfully logged out'+status);
					window.location.href = "/ifill/ifill/views/showAppHomePage";
				},
				error : function(xhr) {
					//alert("Opps !! We could not delete the row now. Please try again. Please find the cause : " + xhr.status);
					$("#errorModal").modal('show');
					$("#errorMsg").html("");
					$("#errorMsg").append("Opps !! Something went wrong at server. Please try again. <br>Please find the cause : " + xhr.status);
				}
			});
		});

					});
					
					var today = new Date();
					var previousDate = new Date(today);
					previousDate.setDate(today.getDate() - 7); // this value has to come from a property file
					
					$('#dayDate').datepicker({
						startDate : previousDate,
						endDate : today,
					    autoclose : true,
					    todayBtn : true
					});
				
			});
			
			function deleteRow(e){
						var id = e.id;
						var tokens = id.split("~");
						
						var key = tokens[1]+ "~" +tokens[2];
						// delete call
						$.ajax({
							type : "DELETE",
							url : "/ifill/ifill/inventory/"+key+"/deleteDailyReadings",
							contentType : 'application/json',
							dataType : "text",
							success : function(status) {												
								document.getElementById(tokens[0]).remove();
								$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Row successfully deleted !!");
							},
							error : function(xhr) {
								//alert("Opps !! We could not delete the row now. Please try again. Please find the cause : " + xhr.status);
								$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Opps !! We could not delete the row now. Please try again. <br>Please find the cause : " + xhr.status);
							}
						});
						
					}
</script>
</head>
<body>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="604" height="30"><span class="logo">I-</span><span
				class="logo1">fill </span></td>
			<td width="399" height="45" align="right" valign="top"><img
				src="${pageContext.request.contextPath}/images/logout.png" alt="logout.png" width="120" height="40" /></td>
		</tr>
	</table>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td><img src="${pageContext.request.contextPath}/images/banner2.jpg" alt="image2" width="1003" height="150" /></td>
		</tr>
	</table>
	<br />
	<table width="945" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="263" height="40" align="left"
				style="background-image: url(${pageContext.request.contextPath}/images/report_button.png); background-repeat: no-repeat"
				class="subheading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Initial Readings</td>
			<td width="740">&nbsp;</td>
		</tr>
	</table>
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column">
			<form action="/ifill/ifill/inventory/postDailyReadings" method="POST" id="dailyReadings" >
				<table class="table table-bordered table-striped" >
					<thead>
						<tr>
							<th class="text-center">Fuel Type<span color='red'>*</span>   </th>
							<th class="text-center">Select a Pump<span color='red'>*</span>   </th>
							<th class="text-center">Date<span color='red'>*</span></th>
							<th class="text-center">Closing Reading (Liters)<span color='red'>*</span></th>
							<th class="text-center">Pump Test (Liters)</th>
						</tr>
					</thead>
					<tbody>
						<tr id='addr0'>
						<td><div class="col-md-12">
									<select class='form-control' id='productType' ><option value="XXX">Select&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option></select>
									</div></td>
							<td><div class="col-md-12">
									<select class='form-control' id='pumpId' name='pumpId' ><option value="XXX">Select&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option></select>
									</div></td>
								<td><div class="col-md-offset-1 col-md-10"><input type="text" class="form-control datepicker" id="dayDate" name="dayDate"  data-date-format="dd-mm-yyyy"/></div></td>
							<td><div class="col-md-offset-1 col-md-10"><input type="text" name='quantity' id='quantity' class="form-control" /></div></td>
							<td><div class="col-md-offset-1 col-md-10"><input type="text" name='pumpTest' id='pumpTest' class="form-control" /></div></td>
							
						</tr>
						<tr id='addr1'></tr>
					</tbody>
				</table>
				<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='submit_row' href="" class="pull-right btn btn-default">Submit Values</a></td></table>
			</form>
			</div>
			
		</div>
		<hr width="35%">
	</div>
	
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column" >
			<table class="table table-condensed table-striped table-bordered " id="tab_logic">
				<thead>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">Pump</td>
						<td class="text-center">Date</td>
						<td class="text-center">Closing Reading</td>
						<td class="text-center">Pump Test</td>
						<td class="text-center">Effective Reading (Liters)</td>
					</tr>
				</thead>
				<tr id="readingsGrid1"></tr>
			</table>
			<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='backbutton' href="/ifill/ifill/views/showAdminHome" class="pull-right btn btn-default">Go Back</a></td></table>
			</div>
		</div>
	</div>
	
<%-- 	<table width="100%">
		<tr height="60">
			<td style="width: 521px; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td height="40" align="right" ><button style="background-image:url(${pageContext.request.contextPath}/images/backbutton.png); background-repeat:no-repeat"><</button></td>
		</tr>
	</table> --%>

	<br />
	<br />
	<br />	
	<br />
	<br />
	<br />
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td height="30" align="center" bgcolor="#8780b0" class="copyright">@copyright
				2013 . All Rights Reserved</td>
		</tr>
	</table>
	<div id="errorModal" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title"><img width="35" height="35" alt="" src="${pageContext.request.contextPath}/images/error-icon.png">&nbsp;&nbsp;Error Alert !! </h4>
                </div>
                <div class="modal-body">
                	<p id='errorMsg'></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>