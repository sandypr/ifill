<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ifill : Admin Reports</title>
<link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
<style type="text/css">
.Text_grey {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #999999;
}
</style>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
					
						(function($) {
								$.fn.serializeObject = function() {
									var self = this, json = {}, push_counters = {}, patterns = {
										"validate" : /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
										"key" : /[a-zA-Z0-9_]+|(?=\[\])/g,
										"push" : /^$/,
										"fixed" : /^\d+$/,
										"named" : /^[a-zA-Z0-9_]+$/
									};
									this.build = function(base, key, value) {
										base[key] = value;
										return base;
									};
									this.push_counter = function(key) {
										if (push_counters[key] === undefined) {
											push_counters[key] = 0;
										}
										return push_counters[key]++;
									};
						$.each($(this).serializeArray(),
							function() {
								// skip invalid keys
								if (!patterns.validate.test(this.name)) {
									return;
								}
								var k, keys = this.name.match(patterns.key), merge = this.value, reverse_key = this.name;
								while ((k = keys.pop()) !== undefined) {
									// adjust reverse_key
									reverse_key = reverse_key.replace(
											new RegExp("\\[" + k + "\\]$"), '');
									// push
									if (k.match(patterns.push)) {
										merge = self.build([], self
												.push_counter(reverse_key),
												merge);
									}
									// fixed
									else if (k.match(patterns.fixed)) {
										merge = self.build([], k, merge);
									}
									// named
									else if (k.match(patterns.named)) {
										merge = self.build({}, k, merge);
									}
								}
								json = $.extend(true, json, merge);
							});
			return json;
		};
	})(jQuery);
						


				$('#submit_row').click(function(e){
					e.preventDefault();
						console.log(JSON.stringify($('#showReport').serializeObject()));
						$.ajax({
								type : "POST",
								url : $('#showReport').attr('action'),
								data : JSON.stringify($('#showReport').serializeObject()),
								contentType : 'application/json',
								dataType : "text",
								success : function(xhr){
									//alert(xhr);
									window.location.href = "/ifill/ifill/reports/"+xhr;
								},
								error : function(xhr) {
									//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
									$("#errorModal").modal("show");
									$("#errorMsg").html("");
									$("#errorMsg").append("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
								}
							});
					
					});
					
					function proceed(){
						alert('success');
						//window.location.href = "/ifill/ifill/reports/showSalesReport";
					}
					
					var startdate = '15-05-2014';
					var today = new Date();
					var endDate = today.toDateString();
					
					//alert(endDate);
					/* $('#date').datepicker({
						startDate : startdate,
						endDate : endDate,
					    autoclose : true,
					    todayBtn : true
					}); */
				
				
		$('#logoutButton').click(function(e){
		e.preventDefault();
			$.ajax({
				type : "GET",
				url : "/ifill/ifill/profile/logout",
				contentType : 'application/json',
				dataType : "text",
				success : function(status) {												
					//alert('successfully logged out'+status);
					window.location.href = "/ifill/ifill/views/showAppHomePage";
				},
				error : function(xhr) {
					//alert("Opps !! We could not delete the row now. Please try again. Please find the cause : " + xhr.status);
					$("#errorModal").modal('show');
					$("#errorMsg").html("");
					$("#errorMsg").append("Opps !! Something went wrong at the server. Please try again. Please find the cause : " + xhr.status);
				}
			});
		});
					
			$('#date').change(function(e){
				$('#endDate').val("");
				var selectedStartDate = $('#date').val();
				$('#endDate').datepicker({
					startDate : selectedStartDate,
					endDate : endDate,
				    autoclose : true,
				    todayBtn : true
				});
			});
			
			
			$('#type').change(function(e){
				var type = $('#type').val();
				// periodic report 
				if(type == 'periodic_report' || type == 'expenseWise_report'){
					$('#label_endDate').html("");
					$('#content_endDate').html("");
					
					$('#date_label').html("<div class='col-md-offset-1 col-md-6'><label for='date' class='col-md-6 control-label' id='startDateLabel'></label></div>");
					$('#startDateLabel').html("From");
					$('#date_holder').html("<div class='col-md-offset-1 col-md-10'><input type='text' class='form-control datepicker' id='date' name='date' /></div>");
					$('#date').datepicker('remove');
					 $('#date').datepicker({
					 	format : "dd-mm-yyyy",
						startDate : startdate,
						endDate : endDate,
					    autoclose : true,
					    todayBtn : true
					});
					
					$('#label_endDate').html("<div class='col-md-offset-1 col-md-6'><label for='endDate' class='col-md-6 control-label'>To</label></div>");
					$('#content_endDate').html("<div class='col-md-offset-1 col-md-10'><input type='text' class='form-control datepicker' id='endDate' name='endDate' data-date-format='dd-mm-yyyy'/></div>");
					$('#endDate').datepicker({
						format : "dd-mm-yyyy",
						startDate : startdate,
						endDate : endDate,
					    autoclose : true,
					    todayBtn : true
					});
				
				// balance report
				}else if(type == 'balance_report' || type == 'invoice_balance_report' ){
				
					// costemic changes
					$('#startDateLabel').html("");
					$("#date_label").html("");
					$("#date_holder").html("");
					$('#label_endDate').html("");
					$('#content_endDate').html("");
					
					$('#label_endDate').html("<div class='col-md-offset-1 col-md-6'><label for='month' class='col-md-6 control-label'>Month&Year</label></div>");
					$('#content_endDate').html("<div class='col-md-offset-1 col-md-10'><input type='text' class='form-control datepicker' id='month' name='month' /></div>");
					
					$('#month').datepicker('remove');
					$("#month").datepicker( {
					    format: "M yyyy",
					    startView: 1,
					    minViewMode: 1,
					    autoclose : true
					});
					
					// data changes
					
				
				// stock report
				}else {
					$("#date_label").html("");
					$("#date_holder").html("");
					$('#date_label').append("<div class='col-md-offset-1 col-md-6'><label for='date' class='col-md-6 control-label' id='startDateLabel'>Date</label></div>");
					$('#date_holder').html("<div class='col-md-offset-1 col-md-10'><input type='text' class='form-control datepicker' id='date' name='date' /></div>");
					
					$('#date').datepicker('remove');
					 $('#date').datepicker({
					 	format : "dd-mm-yyyy",
						startDate : startdate,
						endDate : endDate,
					    autoclose : true,
					    todayBtn : true
					}); 
					$('#label_endDate').html("");
					$('#content_endDate').html("");
				}
			});
					
			});
</script>
</head>
<body>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="604" height="30"><span class="logo">I-</span><span
				class="logo1">fill </span></td>
			<td width="399" height="45" align="right" valign="top"><a href="#" id='logoutButton'><img src="${pageContext.request.contextPath}/images/logout.png" alt="logout.png" width="120" height="40" /></a></td>
		</tr>
	</table>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td><img src="${pageContext.request.contextPath}/images/banner2.jpg" alt="image2" width="1003" height="150" /></td>
		</tr>
	</table>
	<br />
	<table width="945" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="263" height="40" align="left"
				style="background-image: url(${pageContext.request.contextPath}/images/report_button.png); background-repeat: no-repeat"
				class="subheading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Misc Reports</td>
			<td width="740">&nbsp;</td>
		</tr>
	</table>
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column">
			<form method="POST" id="showReport" action="/ifill/ifill/reports/showReport">
				<table class="table table-striped" border='0'>
					<tbody>
						<tr id='addr0'>
							<td><label for='type' class='control-label'>Report Type</label></td>
							<td><div class="col-md-12">
									<select name="type" id="type" class="form-control" >
									<option>Select</option>
									<option value="sales_report">Sales Report</option>
									<option value="periodic_report">Periodic Stock Report</option>
									<option value="balance_report">BalanceSheet Report</option>
									<option value="invoice_balance_report">Company Balance Report</option>
									<option value="expenseWise_report">ExpenseWise Report</option>
								</select>
							</div></td>
							<td id='date_label'><div class='col-md-offset-1 col-md-6'><label for='date' class='col-md-6 control-label' id='startDateLabel'>Date</label></div></td>
							<td id='date_holder'><div class="col-md-12"><input type="text" class="form-control datepicker" id="date" name="date" data-date-format='dd-mm-yyyy'/></div></td>
							<td id='label_endDate'></td>
							<td id='content_endDate'></td>
						</tr>
						<tr id='addr1'></tr>
					</tbody>
				</table>
				<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='submit_row' class="pull-right btn btn-default">&nbsp;&nbsp;&nbsp; Show Report &nbsp;&nbsp;&nbsp;</a></td></tr></table>
			</form>
			</div>
			
		</div>
		<hr width="75%">
	</div>
	
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column" >
			<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='backbutton' href="/ifill/ifill/views/showAdminHome" class="pull-right btn btn-default">Go Back</a></td></table>
			</div>
		</div>
	</div>
			
	<br />
	<br />
	<br />	
	<br />
	<br />
	<br />
		<br />
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td height="30" align="center" bgcolor="#8780b0" class="copyright">@copyright
				2013 . All Rights Reserved</td>
		</tr>
	</table>
	<div id="errorModal" class="modal fade" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title"><img width="35" height="35" alt="" src="${pageContext.request.contextPath}/images/error-icon.png">&nbsp;&nbsp;Error Alert !! </h4>
                </div>
                <div class="modal-body">
                	<p id='errorMsg'></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>