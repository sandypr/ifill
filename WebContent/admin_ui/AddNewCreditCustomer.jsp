<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ifill : Add Credit Customer</title><link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
<style type="text/css">
.Text_grey {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #999999;
}
</style>
<script type="text/javascript">

	$(document)
			.ready(
					function() {
					
						(function($) {
								$.fn.serializeObject = function() {
									var self = this, json = {}, push_counters = {}, patterns = {
										"validate" : /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
										"key" : /[a-zA-Z0-9_]+|(?=\[\])/g,
										"push" : /^$/,
										"fixed" : /^\d+$/,
										"named" : /^[a-zA-Z0-9_]+$/
									};
									this.build = function(base, key, value) {
										base[key] = value;
										return base;
									};
									this.push_counter = function(key) {
										if (push_counters[key] === undefined) {
											push_counters[key] = 0;
										}
										return push_counters[key]++;
									};
						$.each($(this).serializeArray(),
							function() {
								// skip invalid keys
								if (!patterns.validate.test(this.name)) {
									return;
								}
								var k, keys = this.name.match(patterns.key), merge = this.value, reverse_key = this.name;
								while ((k = keys.pop()) !== undefined) {
									// adjust reverse_key
									reverse_key = reverse_key.replace(
											new RegExp("\\[" + k + "\\]$"), '');
									// push
									if (k.match(patterns.push)) {
										merge = self.build([], self
												.push_counter(reverse_key),
												merge);
									}
									// fixed
									else if (k.match(patterns.fixed)) {
										merge = self.build([], k, merge);
									}
									// named
									else if (k.match(patterns.named)) {
										merge = self.build({}, k, merge);
									}
								}
								json = $.extend(true, json, merge);
							});
			return json;
		};
	})(jQuery);
	
					// validations
					$('#name').blur(function(e){
						var name = $('#name').val();
						if(name.length >= 3 )
							checkCCName(name);
					});
					
					function checkCCName(name){
						$.ajax({
								type : "GET",
								url : "/ifill/ifill/profile/isCCNameUnique/"+name,
								contentType : 'application/json',
								dataType : "text",
								success : function(status) {												
									if(status == 'false'){
										$("#errorModal").modal('show');
										$("#errorMsg").html("");
										$("#errorMsg").append("Credit Customer name is already in use. Please choose another name.");
										//$('#name').focus();
										return false;
									}else{
										return true;
									}
								},
								error : function(xhr) {
									$("#errorModal").modal('show');
									$("#errorMsg").html("");
									$("#errorMsg").append("Opps !! Something went wrong at the server. Please try again. Please find the cause : " + xhr.status);
									return false;
								}
							});
					}
					
					$('#mobile').change(function(e){
						var val = $('#mobile').val();
						if(val.length < 1 && val.length > 11 || !$.isNumeric(val)){
							//alert('Please provide a valid number for Contact.');
							$("#errorModal").modal('show');
							$("#errorMsg").html("");
							$("#errorMsg").append("Please provide a valid number for Contact.");
						};
					});
					
					
					// Submision of values
					var readings = 1;		
					var KEY_SEPERATOR = "~";			
					$('#submit_row').click(function(e){
						var mobile = $('#mobile').val();
						var name = $('#name').val();
						var address = $('#address').val();
						var date = $('#date_created').val();
						var initial_balance = $('#initialBalance').val();
						e.preventDefault();
						if(name.length < 3 
							|| address.length < 4 
								|| (mobile.length < 6)
								|| !$.isNumeric(initial_balance) ){
							//alert('Please provide correct values for all the fields before submitting.');
							$("#errorModal").modal("show");
							$("#errorMsg").html("");
							$("#errorMsg").append("Please provide correct values for all the fields before submitting.");
						}else {
						
							// check uniqueness of name
							var isUnique = checkCCName(name);
							if(isUnique == 'false') return false;
							
							console.log(JSON.stringify($('#creditCustomer').serializeObject()));
							
							$.ajax({
								type : "POST",
								url : $('#creditCustomer').attr('action'),
								data : JSON.stringify($('#creditCustomer').serializeObject()),
								contentType : 'application/json',
								dataType : "text",
								success : function(status) {
								
									if(status == -1){
										//alert("Your session is timedout or you haven't logged in.");
										$("#errorModal").modal('show');
										$("#errorMsg").html("");
										$("#errorMsg").append("Your session is timedout or you haven't logged in.");
										window.href="/ifill/ifill/views/showAppHomePage";
									}
									
									var key = "readingsGrid"+readings+ KEY_SEPERATOR +name;
									$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'><a href='#' id="+key+" name="+key+"  onClick='deleteRow(this)'><img src='${pageContext.request.contextPath}/images/delete_btn.png' alt='delete_btn.png' width='15' height='15'/> </a></div></td>");
						
									$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+name+"</div></td>");
									$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+address+"</div></td>");
									$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+mobile+"</div></td>");
									$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+initial_balance+"</div></td>");
									
									readings = readings + 1;
									$('#tab_logic').append("<tr id='readingsGrid"+readings+"' ></tr>");
									
									// clear values
									$('#name').val("");
									$('#mobile').val("");
									$('#address').val("");
									$('#initialBalance').val("");
								},
								error : function(xhr) {
										//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
										$("#errorModal").modal('show');
										$("#errorMsg").html("");
										$("#errorMsg").append("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
								}
							});
							return false;
						}
					});
					
		$('#logoutButton').click(function(e){
			e.preventDefault();
			$.ajax({
				type : "GET",
				url : "/ifill/ifill/profile/logout",
				contentType : 'application/json',
				dataType : "text",
				success : function(status) {												
					//alert('successfully logged out'+status);
					window.location.href = "/ifill/ifill/views/showAppHomePage";
				},
				error : function(xhr) {
					//alert("Opps !! We could not delete the row now. Please try again. Please find the cause : " + xhr.status);
					$("#errorModal").modal('show');
					$("#errorMsg").html("");
					$("#errorMsg").append("Opps !! Something went wrong at the server. Please try again. Please find the cause : " + xhr.status);
				}
			});
		});
					
			});
			
			function deleteRow(e){
						var id = e.id;
						var tokens = id.split("~");
						var key = tokens[1];
						// delete call
						$.ajax({
							type : "DELETE",
							url : "/ifill/ifill/admin/"+key+"/deleteCreditCustomer",
							contentType : 'application/json',
							dataType : "text",
							success : function(status) {												
								document.getElementById(tokens[0]).remove();
								//alert('Row successfully deleted !!');
								$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Row successfully deleted !!");
							},
							error : function(xhr) {
								//alert("Opps !! We could not delete the row now. Please try again. Please find the cause : " + xhr.status);
								$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Opps !! We could not delete the row now. Please try again. Please find the cause : " + xhr.status);
							}
						});
						
					};
</script>
</head>
<body>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="604" height="30"><span class="logo">I-</span><span
				class="logo1">fill </span></td>
			<td width="399" height="45" align="right" valign="top"><a href="#" id='logoutButton'><img src="${pageContext.request.contextPath}/images/logout.png" alt="logout.png" width="120" height="40" /></a></td>

		</tr>
	</table>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td><img src="${pageContext.request.contextPath}/images/banner2.jpg" alt="image2" width="1003" height="150" /></td>
		</tr>
	</table>
	<br />
	<table width="945" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="263" height="40" align="left"
				style="background-image: url(${pageContext.request.contextPath}/images/report_button.png); background-repeat: no-repeat"
				class="subheading">&nbsp;&nbsp;Create Credit Customer</td>
			<td width="740">&nbsp;</td>
		</tr>
	</table>
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column">
			<form action="/ifill/ifill/admin/createCreditCustomer" method="POST" id="creditCustomer" >
				<table class="table table-bordered table-striped" >
					<thead>
						<tr>
							<th class="text-center">Name<span color='red'>*</span></th>
							<th class="text-center">Address<span color='red'>*</span></th>
							<th class="text-center">Initial Balance<span color='red'>*</span></th>
							<th class="text-center">Contact Number<span color='red'>*</span></th>
						</tr>
					</thead>
					<tbody>
						<tr id='addr0'>
							<td valign="middle"><div class="col-md-offset-1 col-md-10"><input type="text" name='name' id='name' class="form-control" /></div></td>
							<td valign="middle"><div class="col-md-offset-1 col-md-10">
									<textarea rows="3" cols="25" id='address' name='address' class="form-control" ></textarea>
								</div>
							</td>
							<td valign="middle"><div class="col-md-offset-1 col-md-10">
									<input type="text" id='initialBalance' name='initialBalance' class="form-control" />	
								</div>
							</td>
							<td valign="middle"><div class="col-md-offset-1 col-md-10">
									<input type="text" id='mobile' name='mobile' class="form-control" />	
								</div>
							</td>
						</tr>
						<tr id='addr1'></tr>
					</tbody>
				</table>
				<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='submit_row' href="" class="pull-right btn btn-default">Submit Values</a></td></table>
			</form>
			</div>
			
		</div>
		<hr width="35%">
	</div>
	
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column" >
			<table class="table table-condensed table-striped table-bordered " id="tab_logic">
				<thead> <tr>
					<th class="text-center"></th>
					<th class="text-center">Name</th>
					<th class="text-center">Address</th>
					<th class="text-center">Mobile</th>
					<th class="text-center">Initial Balance</th>
				</tr></thead>
				<tr id="readingsGrid1"></tr>
			</table>
			<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='backbutton' href="/ifill/ifill/views/showAdminHome" class="pull-right btn btn-default">Go Back</a></td></table>
			</div>
		</div>
	</div>

	<br />
	<br />
	<br />	
	<br />
	<br />
	<br />
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td height="30" align="center" bgcolor="#8780b0" class="copyright">@copyright
				2013 . All Rights Reserved</td>
		</tr>
	</table>
	<div id="errorModal" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title"><img width="35" height="35" alt="" src="${pageContext.request.contextPath}/images/error-icon.png">&nbsp;&nbsp;Error Alert !! </h4>
                </div>
                <div class="modal-body">
                	<p id='errorMsg'></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>