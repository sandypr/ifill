<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ifill : Stock Receipt</title>
<link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
<style type="text/css">
.Text_grey {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #999999;
}
</style>
<script type="text/javascript">
	$(document).ready(function(){
	
		var rowList = {};
		$.ajax({
				type : "GET",
				url : "/ifill/ifill/reports/showBalanceData",
				contentType : 'application/json',
				dataType : "text",
				success : function(rows) {												
					var finalData = jQuery.parseJSON(rows);
					rowList = finalData;
					paintGrid('page1');
					
					// pagination handling
					handlePagination();					
				},
				error : function(xhr) {
					alert("Opps !! We could not delete the row now. Please try again. Please find the cause : "
							+ xhr.status);
				}
			});
			
		var currentPage = "";
		function paintGrid(page){
			currentPage = page;
			$('#balanceData').html("");
			$('#balanceData').append("<thead> <th>Date</th> <th>Cash Position</th> <th>Consolidate Cash</th> <th>Rs Deposited in Bank</th> <th>Rs Tranfered to Company</th> <th>New Amounts to Mgr</th> <th>Balance with Mgr</th></thead>");
			
			var index = 0;
			var rowsInAPage = 10;
			
			if(page == 'page3')
				index = 19;
			else if(page == 'page2')
				index = 9;
				
			$('#balanceData').append("<tr id='row"+index+"'> </tr>");
			//alert(rowList.length);
			var maxRows = index+rowsInAPage;
			//alert('starting with : '+index);
			while(index < maxRows && index < rowList.length) {

				//alert(index + "-" +rowList.length);
				var row = rowList[index];
				console.log(row);
				var consolidate = Number(row['balanceYesterday'])+Number(row['sale']);

				$('#row'+index).append("<td><div class='col-md-12'>"+row['date']+"</div></td>");
				$('#row'+index).append("<td><div class='col-md-offset-1 col-md-10'>"+row['sale']+"</div></td>");
				$('#row'+index).append("<td><div class='col-md-offset-1 col-md-10'>"+consolidate+"</div></td>");
				$('#row'+index).append("<td><div class='col-md-offset-1 col-md-10'>"+row['rsDeposited']+"</div></td>");
				$('#row'+index).append("<td><div class='col-md-offset-1 col-md-10'>"+row['rsTransferred']+"</div></td>");
				$('#row'+index).append("<td><div class='col-md-offset-1 col-md-10'>"+row['newAddition']+"</div></td>");
				$('#row'+index).append("<td><div class='col-md-offset-1 col-md-10'>"+row['availableBal']+"</div></td>");
				
				$('#balanceData').append("<tr id='row"+(index+1)+"'> </tr>");
				index++;
			};
			//alert('ending at : '+index);
			
			// pagination handling
			handlePagination();	
		}
		
		function handlePagination(){
			if(currentPage == 'page1'){
				//alert('o its page1');
				$('#page2').removeClass("active");
				$('#page3').removeClass("active");
				
				$('#previous').addClass("disabled");
				$('#page1').addClass("active");
				
				if(rowList.length < 11){
					$('#page2').addClass("disabled");
					$('#page3').addClass("disabled");
					$('#next').addClass("disabled");
				}else if(rowList.length < 21){
					$('#page3').addClass("disabled");
					$('#next').addClass("disabled");
				}
					
				
			}else if(currentPage == 'page2'){
				//alert('o its page2');
				$('#previous').removeClass("disabled");
				$('#page1').removeClass("active");
				$('#page3').removeClass("active");
				$('#page2').addClass("active");
				
				if(rowList.length > 20){
					$('#next').removeClass("disabled");
					$('#page3').addClass("active");
				}
				
			}else {
				//alert('o its page3');
				$('#next').addClass("disabled");
				$('#page1').removeClass("active");
				$('#page2').removeClass("active");
				$('#previous').removeClass("disabled");
				$('#page3').addClass("active");
			}
		}
		
		$('#page1').on('click', function(){
			//alert('clicked on page 1');
			if(!$('#page1').hasClass("disabled"))
				paintGrid("page1");
		});
		
		$('#page2').on('click', function(){
			//alert('clicked on page 2');
			if(!$('#page2').hasClass("disabled"))
				paintGrid("page2");
		});
		
		$('#page3').on('click', function(){
			//alert('clicked on page 3');
			if(!$('#page3').hasClass("disabled"))
				paintGrid("page3");
		});
		
		$('#previous').on('click', function(){
			if(!$('#previous').hasClass("disabled")){
				//alert('clicked on previous');
				if(currentPage == 'page2')
					paintGrid("page1");
				else if(currentPage == 'page3')
					paintGrid("page2");
			}
		});
		
		$('#next').on('click', function(){
			if(!$('#next').hasClass("disabled")){
				//alert('clicked on next');
				if(currentPage == 'page2')
					paintGrid("page3");
				else if(currentPage == 'page1')
					paintGrid("page2");
				}
		});
	});
</script>
</head>
<body>

<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="604" height="30"><span class="logo">I-</span><span
				class="logo1">fill </span></td>
			<td width="399" height="45" align="right" valign="top"><a href="#" id='logoutButton'><img src="${pageContext.request.contextPath}/images/logout.png" alt="logout.png" width="120" height="40" /></a></td>
		</tr>
	</table>
	
	<br />
<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="252" height="40" align="left" valign="middle" class="subheading" style="background-image:url(${pageContext.request.contextPath}/images/report_button.png); background-repeat:no-repeat">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Balance Sheet</td>
    <td width="751"><br />
   </td>
  </tr>
</table>

<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="887">
    <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #cccccc;
    border-radius:5px; background-image:url(${pageContext.request.contextPath}/images/login_bg.jpg); background-repeat:repeat" bgcolor="#f7f7f7">
      <tr>
        <td height="42" align="center" valign="middle"></br>
          <table>
				<tr align="center"><td><h3>ABC Filling Station, </br></h3><h4>PQR Road, XYZ Dist.</h4></td></tr>
				<!-- <tr align="center"><td><h4>Tanikella Road, Khammam Dist.</h4></td></tr> -->							
			</table> 
			<table width="980" border="0" align="center" cellpadding="0"
				cellspacing="0">
				<tr align="left">
					<td width="418"></td>
					<td width="562" height="25" align="center" valign="bottom">
							&nbsp;&nbsp;<span class="para5">Date : <u><%-- <%= session.getAttribute("Date").toString(); %> --%></u></span>
						</td>
				</tr>
			</table><hr width="75%" align="center" /></br>                
            <div class="col-md-offset-1 col-md-10 column">
            	<table class='table table-condensed table-striped table-bordered' id='balanceData'>
            			<tr>
            				<th>Date</th>
            				<th>Cash Position</th>
            				<th>Consolidate Cash</th>
            				<th>Rs Deposited in Bank</th>
            				<th>Rs Tranfered to Company</th>
            				<th>New Amounts to Mgr</th>
            				<th>Balance with Mgr</th>
            			</tr>
            	</table>
            </div>
				<ul class="pagination">
				  <li id='previous'><a href="#">&laquo;</a></li>
				  <li id='page1'><a href="#" >1</a></li>
				  <li id='page2'><a href="#">2</a></li>
				  <li id='page3'><a href="#">3</a></li>
				  <li id='next'><a href="#">&raquo;</a></li>
				</ul>  
		<br />
		<div class="col-md-offset-1 col-md-10 column" id="test">
							<table ><tr><td align="left"></td>
								<td align="right"></td>
								<td  class="col-md-offset-1 col-md-10" ></td>
								<td  class="col-md-offset-1 col-md-10" ><a id='saveButton' href="#" class="pull-left btn btn-success" onclick="divPrint();">Print / Save</a></td>
								<td><a id='backbutton' href="/ifill/ifill/views/showAdminHome" class="pull-left btn btn-warning">Go Back</a></td>
								</table>
								<script type="text/javascript">
							      function divPrint() {
							      	//alert("printing");
							        //$("#print_div").addClass("printable");
							        //$('body').css('display','none');
							        
							        //$('#print_div').css('display', 'block');
							        //alert('added printable');
							        $('#logout_button').css('display','none');
							        $('#saveButton').css('display','none');
							        $('#backbutton').css('display','none');
							        window.print();
							      }
							    </script>
						</div>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<br />
<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" align="center" bgcolor="#8780b0" class="copyright">@copyright 2013 . All Rights Reserved</td>
  </tr>
</table>
</body>
</html>