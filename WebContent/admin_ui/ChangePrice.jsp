<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ifill : Change Price</title>
<link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
<style type="text/css">
.Text_grey {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #999999;
}
</style>
<script type="text/javascript">

	$(document)
			.ready(
					function() {
					
						// writing on load. 
						// pull products and populate the form
						$.ajax({
							type : "GET",
							url : "/ifill/ifill/metadata/products",
							contentType : 'application/json',
							dataType : "text",
							success : function(ldr) {
								var finalData = jQuery.parseJSON(ldr);
								$.each(finalData, function(index, value) {
								$("#productId").append(
										'<option value="'+value.productId+'">' + value.name
												+ '</option>');
								
								});
							},
							error : function(xhr) {
								//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
								$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Opps !!!! Somethings went wrong at the server.  <br>Please contact admin for further help : " + xhr.status);
							}
						});
						
					
						(function($) {
								$.fn.serializeObject = function() {
									var self = this, json = {}, push_counters = {}, patterns = {
										"validate" : /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
										"key" : /[a-zA-Z0-9_]+|(?=\[\])/g,
										"push" : /^$/,
										"fixed" : /^\d+$/,
										"named" : /^[a-zA-Z0-9_]+$/
									};
									this.build = function(base, key, value) {
										base[key] = value;
										return base;
									};
									this.push_counter = function(key) {
										if (push_counters[key] === undefined) {
											push_counters[key] = 0;
										}
										return push_counters[key]++;
									};
						$.each($(this).serializeArray(),
							function() {
								// skip invalid keys
								if (!patterns.validate.test(this.name)) {
									return;
								}
								var k, keys = this.name.match(patterns.key), merge = this.value, reverse_key = this.name;
								while ((k = keys.pop()) !== undefined) {
									// adjust reverse_key
									reverse_key = reverse_key.replace(
											new RegExp("\\[" + k + "\\]$"), '');
									// push
									if (k.match(patterns.push)) {
										merge = self.build([], self
												.push_counter(reverse_key),
												merge);
									}
									// fixed
									else if (k.match(patterns.fixed)) {
										merge = self.build([], k, merge);
									}
									// named
									else if (k.match(patterns.named)) {
										merge = self.build({}, k, merge);
									}
								}
								json = $.extend(true, json, merge);
							});
			return json;
		};
	})(jQuery);
	
					// validations
					// Submision of values
					var readings = 1;		
					var KEY_SEPERATOR = "~";			
					$('#submit_row').click(function(e){
						var productId = $('#productId').val();
						var invoicePrice = $('#invoicePrice').val();
						var unitPrice = $('#unitPrice').val();
						var date = $('#dayDate').val();
						var oldInvoicePrice = $('#currentIPrice').val();
						var oldUnitPrice = $('#currentUPrice').val();
						e.preventDefault();
						if(productId == 'XXX' || invoicePrice.length < 1 || invoicePrice.length < 1){
							//alert('Please provide values for all the fields before submitting.');
							$("#errorModal").modal('show');
							$("#errorMsg").html("");
							$("#errorMsg").append("Please provide values for all the fields before submitting.");
						}else if (!$.isNumeric(invoicePrice)){
							//alert('Please enter a valid numeric value for Invoice Price.');
							$("#errorModal").modal('show');
							$("#errorMsg").html("");
							$("#errorMsg").append('Please enter a valid numeric value for Invoice Price.');
						}else if (!$.isNumeric(unitPrice)) {
							//alert('Please enter a valid numeric value for Unit Price');
							$("#errorModal").modal('show');
							$("#errorMsg").html("");
							$("#errorMsg").append('Please enter a valid numeric value for Unit Price');
						}else {

							console.log(JSON.stringify($('#adminOperations').serializeObject()));
							$.ajax({
								type : "POST",
								url : $('#adminOperations').attr('action'),
								data : JSON.stringify($('#adminOperations').serializeObject()),
								contentType : 'application/json',
								dataType : "text",
								success : function(status) {
									
									if(status == 'Success'){
										var key = "readingsGrid"+readings+ KEY_SEPERATOR +productId+ KEY_SEPERATOR +date;
										$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'><a href='#' id="+key+" name="+key+"  onClick='deleteRow(this)'><img src='${pageContext.request.contextPath}/images/delete_btn.png' alt='delete_btn.png' width='15' height='15'/> </a></div></td>");
							
										$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+productId+"</div></td>");	
										$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+oldInvoicePrice+"</div></td>");								
										$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+invoicePrice+"</div></td>");
										$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+oldUnitPrice+"</div></td>");
										$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+unitPrice+"</div></td>");
										
										readings = readings + 1;
										$('#tab_logic').append("<tr id='readingsGrid"+readings+"' ></tr>");
										
										// clear values
										$('#productId').val("XXX");
										$('#invoicePrice').val("");
										$('#unitPrice').val("");
										$('#currentIPrice').val("");
										$('#currentUPrice').val("");						
									
									}
									
								},
								error : function(xhr) {
									//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
									$("#errorModal").modal('show');
									$("#errorMsg").html("");
									$("#errorMsg").append("Opps !!!! Somethings went wrong at the server. <br>Please contact admin for further help : " + xhr.status);
								}
							});
							return false;
						}
					

					});
					
					$('#productId').change(
										function(event) {
						var id = this.value;
						//alert('selected a productv: '+id);
						
						$.ajax({
							type : "GET",
							url : "/ifill/ifill/metadata/"+id+"/itemprice",
							contentType : 'application/json',
							dataType : "text",
							success : function(price) {		
								var finalData = jQuery.parseJSON(price);
								console.log(finalData);										
								$('#currentUPrice').val(finalData.UnitPrice);
								$('#currentIPrice').val(finalData.InvoicePrice);
								$('#invoicePrice').val("");
								$('#unitPrice').val("");
							},
							error : function(xhr) {
								//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
								$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Opps !!!! Somethings went wrong at the server.  <br>Please contact admin for further help : " + xhr.status);
							}
						});	
										
					});
					
					
			$('#logoutButton').click(function(e){
				e.preventDefault();
				$.ajax({
					type : "GET",
					url : "/ifill/ifill/profile/logout",
					contentType : 'application/json',
					dataType : "text",
					success : function(status) {												
						//alert('successfully logged out'+status);
						window.location.href = "/ifill/ifill/views/showAppHomePage";
					},
					error : function(xhr) {
						//alert("Opps !! Your session is expired. Redirecting to login page.");
						$("#errorModal").modal('show');
						$("#errorMsg").html("");
						$("#errorMsg").append("Opps !! Your session is expired. Redirecting to login page.");
						window.location.href = "/ifill/ifill/views/showAppHomePage";
					}
				});
		});
				
			});
			
			function deleteRow(e){
						var id = e.id;
						var tokens = id.split("~");
						
						var key = tokens[1]+ "~" +tokens[2];
						// delete call
						$.ajax({
							type : "DELETE",
							url : "/ifill/ifill/inventory/"+key+"/deleteDailyReadings",
							contentType : 'application/json',
							dataType : "text",
							success : function(status) {												
								document.getElementById(tokens[0]).remove();
								alert('Row successfully deleted !!');
							},
							error : function(xhr) {
								alert("Opps !! We could not delete the row now. Please try again.  <br>Please find the cause : "
										+ xhr.status);
							}
						});
						
					}
</script>
</head>
<body>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="604" height="30"><span class="logo">I-</span><span
				class="logo1">fill </span></td>
			<td width="399" height="45" align="right" valign="top"><a href="#" id='logoutButton'><img src="${pageContext.request.contextPath}/images/logout.png" alt="logout.png" width="120" height="40" /></a></td>
		</tr>
	</table>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td><img src="${pageContext.request.contextPath}/images/banner2.jpg" alt="image2" width="1003" height="150" /></td>
		</tr>
	</table>
	<br />
	<table width="945" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="263" height="40" align="left"
				style="background-image: url(${pageContext.request.contextPath}/images/report_button.png); background-repeat: no-repeat"
				class="subheading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Change Price</td>
			<td width="740">&nbsp;</td>
		</tr>
	</table>
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column">
			<form action="/ifill/ifill/admin/changePrice" method="POST" id="adminOperations" >
				<table class="table table-bordered table-striped" >
					<thead>
						<tr>
							<th class="text-center">Select a Product<span color='red'>*</span>  </th>
							<th class="text-center">Current Invoice Price</th>
							<th class="text-center">New Invoice Price<span color='red'>*</span></th>
							<th class="text-center">Current Unit Price</th>
							<th class="text-center">New Unit Price<span color='red'>*</span></th>
						</tr>
					</thead>
					<tbody>
						<tr id='addr0'>
							<td><div class="col-md-12">
									<select class='form-control' id='productId' name='productId' ><option value="XXX">Select&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option></select>
									</div></td>
							<td><div class="col-md-12"><input type="text" class="form-control" id="currentIPrice" name="currentIPrice" disabled='disabled'/></div></td>
							<td><div class="col-md-12"><input type="text" name='invoicePrice' id='invoicePrice' class="form-control"/></div></td>
							<td><div class="col-md-12"><input type="text" class="form-control" id="currentUPrice" name="currentUPrice" disabled='disabled'/></div></td>
							<td><div class="col-md-12">
									<input type="text" name='unitPrice' id='unitPrice' class="form-control" />
								</div>
							</td>
						</tr>
						<tr id='addr1'></tr>
					</tbody>
				</table>
				<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='submit_row' href="" class="pull-right btn btn-default">Update Values</a></td></table>
			</form>
			</div>
			
		</div>
		<hr width="35%">
	</div>
	
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column" >
			<table class="table table-condensed table-striped table-bordered " id="tab_logic">
				<thead><tr>
					<th class="text-center">Product</th>
					<th class="text-center">Current Invoice Price</th>
					<th class="text-center">New Invoice Price</th>
					<th class="text-center">Current Unit Price</th>
					<th class="text-center">New Invoice Price</th>
				</tr></thead>
				<tr id="readingsGrid1"></tr>
			</table>
			<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='backbutton' href="/ifill/ifill/views/showAdminHome" class="pull-right btn btn-default">Go Back</a></td></table>
			</div>
		</div>
	</div>

	<br />
	<br />
	<br />	
	<br />
	<br />
	<br />
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td height="30" align="center" bgcolor="#8780b0" class="copyright">@copyright
				2013 . All Rights Reserved</td>
		</tr>
	</table>
	<div id="errorModal" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title"><img width="35" height="35" alt="" src="${pageContext.request.contextPath}/images/error-icon.png">&nbsp;&nbsp;Error Alert !! </h4>
                </div>
                <div class="modal-body">
                	<p id='errorMsg'></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>