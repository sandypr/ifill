<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ifill : Lubes Sale</title>
<link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
<style type="text/css">
.Text_grey {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #999999;
}
</style>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
					
						(function($) {
								$.fn.serializeObject = function() {
									var self = this, json = {}, push_counters = {}, patterns = {
										"validate" : /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
										"key" : /[a-zA-Z0-9_]+|(?=\[\])/g,
										"push" : /^$/,
										"fixed" : /^\d+$/,
										"named" : /^[a-zA-Z0-9_]+$/
									};
									this.build = function(base, key, value) {
										base[key] = value;
										return base;
									};
									this.push_counter = function(key) {
										if (push_counters[key] === undefined) {
											push_counters[key] = 0;
										}
										return push_counters[key]++;
									};
						$.each($(this).serializeArray(),
							function() {
								// skip invalid keys
								if (!patterns.validate.test(this.name)) {
									return;
								}
								var k, keys = this.name.match(patterns.key), merge = this.value, reverse_key = this.name;
								while ((k = keys.pop()) !== undefined) {
									// adjust reverse_key
									reverse_key = reverse_key.replace(
											new RegExp("\\[" + k + "\\]$"), '');
									// push
									if (k.match(patterns.push)) {
										merge = self.build([], self
												.push_counter(reverse_key),
												merge);
									}
									// fixed
									else if (k.match(patterns.fixed)) {
										merge = self.build([], k, merge);
									}
									// named
									else if (k.match(patterns.named)) {
										merge = self.build({}, k, merge);
									}
								}
								json = $.extend(true, json, merge);
							});
			return json;
		};
	})(jQuery);
				
				$('#date').on('change', function(){
					//alert('date changed');
					var dateSelected = $('#date').val();
					
					// load sales and yesterday's balances
					$.ajax({
						type : "GET",
						url : "/ifill/ifill/admin/balanceInits/"+dateSelected,
						contentType : 'application/json',
						dataType : "text",
						success : function(ldr) {
							var finalData = jQuery.parseJSON(ldr);
							var reports = finalData.reports;
							console.log(finalData);
							if(finalData.status == 'false'){
								
								if(reports['sale'] != null && reports['previous'] == null){
									var kontinue = confirm('There is no previous balance data. The data will be considered new from this point. Do you want to continue ?');
									if(kontinue == false)
										window.location.href = "";
									
								}else	{							
									//alert('There is no sales data. Please fill DailySales and expense details before filling Balance information.');
									$("#errorModal").modal('show');
									$("#errorMsg").html("");
									$("#errorMsg").append('There is no sales data. Please fill DailySales and expense details before filling Balance information.');
								}
							} else {
								$('#yesterdays').val(reports["previous"]);
								$('#balanceYesterday').val(reports["previous"]);
								$('#sale').val(reports["sale"]);
								$('#sales').val(reports["sale"]);
							}
						},
						error : function(xhr) {
							//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
							$("#errorModal").modal('show');
							$("#errorMsg").html("");
							$("#errorMsg").append("Opps !!!! Somethings went wrong at the server. <br>Please contact admin for further help : " + xhr.status);
						}
					});
					
					
				});
					
				function calculateAvailableBal(){
					//alert('calculating available bal');
					var deposited = $('#rsDeposited').val();
					var transferred = $('#rsTransferred').val();
					var fresh = $('#newAddition').val();
					var sale = $('#sale').val();
					var yesterdays = $('#yesterdays').val();
					var availableBal = 0.0;
					
					if(deposited == null || !$.isNumeric(deposited)) deposited = 0.0;
					if(transferred == null || !$.isNumeric(transferred)) transferred = 0.0;
					if(fresh == null || !$.isNumeric(fresh)) fresh = 0.0;
					if(sale == null || !$.isNumeric(sale)) sale = 0.0;
					if(yesterdays == null || !$.isNumeric(yesterdays)) yesterdays=0.0;
					
					//alert(yesterdays +"-"+ sale +"-"+ deposited +"-"+ transferred +"-"+ fresh);
					//alert(yesterdays+sale+fresh);
					//alert(deposited+transferred);
					
					availableBal = (Number(yesterdays)+Number(sale)+Number(fresh))-(Number(deposited)+Number(transferred));
					
					$('#available').val(availableBal);
					$('#availableBal').val(availableBal);
				}
						
				$('#rsDeposited').blur(function(e){
					var val = $('#rsDeposited').val();
					if(val.length < 1 || !$.isNumeric(val)){
						//alert('Please provide a valid number : for Rs Deposited.');
						$("#errorModal").modal('show');
						$("#errorMsg").html("");
						$("#errorMsg").append('Please provide a valid number : for Rs Deposited.');
					}else{
						calculateAvailableBal();
					}
				});
				
				$('#rsTransferred').blur(function(e){
					var val = $('#rsTransferred').val();
					if(val.length < 1 || !$.isNumeric(val)){
						//alert('Please provide a valid number for : Rs Transfered to Company.');
						$("#errorModal").modal('show');
						$("#errorMsg").html("");
						$("#errorMsg").append('Please provide a valid number for : Rs Transfered to Company.');
					}else{
						calculateAvailableBal();
					}
				});
						
				$('#newAddition').blur(function(e){
					var val = $('#newAddition').val();
					if(val.length > 0 && !$.isNumeric(val)){
						//alert('Please provide a valid number for : Fresh Amount to Manager.');
						$("#errorModal").modal('show');
						$("#errorMsg").html("");
						$("#errorMsg").append('Please provide a valid number for : Fresh Amount to Manager.');						
					}else{
						calculateAvailableBal();
					}
				});

				$('#submit_row').click(function(e){
				
						var deposited = $('#rsDeposited').val();
						var transferred = $('#rsTransferred').val();
						var fresh = $('#newAddition').val();
						var date = $('#date').val();
						e.preventDefault();
						if(deposited.length < 1 || !$.isNumeric(deposited) || 
								transferred.length < 1 || !$.isNumeric(transferred) || 
								fresh.length < 1 || !$.isNumeric(fresh) || 
								date.length < 1){
							//alert('Please fill all valid values before submitting.');
						$("#errorModal").modal('show');
						$("#errorMsg").html("");
						$("#errorMsg").append('Please fill all valid values before submitting.');								
						}else{
							// now submit values
							console.log(JSON.stringify($('#balances').serializeObject()));
							$.ajax({
								type : "POST",
								url : $('#balances').attr('action'),
								data : JSON.stringify($('#balances').serializeObject()),
								contentType : 'application/json',
								dataType : "text",
								success : proceed,
								error : function(xhr) {
									//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
									$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
									console.log(xhr);
								}
							});
						}
					});

					var readings = 1;
					var KEY_SEPERATOR = "~";
					function proceed(response) {
						//alert('submited values');
						//alert(response);
						var deposited = $('#rsDeposited').val();
						var transferred = $('#rsTransferred').val();
						var fresh = $('#newAddition').val();
						var sale = $('#sale').val();
						var availableBal = $('#availableBal').val();
						var date = $('#date').val();
						var yesterdays = $('#yesterdays').val();
						
						//alert(readings);
						var key = "readingsGrid"+readings+ KEY_SEPERATOR +date;
						$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'><a href='#' id="+key+" name="+key+"  onClick='deleteRow(this)'><img src='${pageContext.request.contextPath}/images/delete_btn.png' alt='delete_btn.png' width='15' height='15'/> </a></div></td>");
						
						$('#readingsGrid'+readings).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+date+"</div></td>");
						$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+yesterdays+"</div></td>");
						$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+sale+"</div></td>");
						$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+deposited+"</div></td>");
						$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+transferred+"</div></td>");
						$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+fresh+"</div></td>");
						$('#readingsGrid'+readings).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+availableBal+"</div></td>");
						
						readings = readings + 1;
						//alert(readings);
						$('#tab_logic').append("<tr id='readingsGrid"+readings+"' ></tr>");
						
						$('#rsDeposited').val("");
						$('#rsTransferred').val("");
						$('#newAddition').val("");
						$('#sale').val("");
						$('#availableBal').val("");
						$('#yesterdays').val("");
						$('#date').val("");
					}

					var today = new Date();
					var previousDate = new Date(today);
					previousDate.setDate(today.getDate() - 7);
					
					$('#date').datepicker({
						startDate : previousDate,
						endDate : today,
					    autoclose : true,
					    todayBtn : true
					});
					
							
		$('#logoutButton').click(function(e){
			e.preventDefault();
			$.ajax({
				type : "GET",
				url : "/ifill/ifill/profile/logout",
				contentType : 'application/json',
				dataType : "text",
				success : function(status) {												
					//alert('successfully logged out'+status);
					window.location.href = "/ifill/ifill/views/showAppHomePage";
				},
				error : function(xhr) {
					//alert("Opps !! We could not delete the row now. Please try again. Please find the cause : " + xhr.status);
					$("#errorModal").modal('show');
					$("#errorMsg").html("");
					$("#errorMsg").append("Opps !! Something went wrong at server. Please try again. <br>Please find the cause : " + xhr.status);
				}
			});
		});
					
					});
					
			function deleteRow(e){
						e.preventDefault();
						var id = e.id;
						var tokens = id.split("~");
						
						var key = tokens[1];//+ "~" +tokens[2];
						// delete call
						$.ajax({
							type : "DELETE",
							url : "/ifill/ifill/inventory/"+key+"/deleteLubeSales",
							contentType : 'application/json',
							dataType : "text",
							success : function(status) {												
								document.getElementById(tokens[0]).remove();								
								alert('Row successfully deleted !!');
							},
							error : function(xhr) {
								//alert("Opps !! We could not delete the row now. Please try again. Please find the cause : " + xhr.status);
								$("#errorModal").modal('show');
								$("#errorMsg").html("");
								$("#errorMsg").append("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : " + xhr.status);
							}
						});
						
					}
</script>
</head>
<body>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="604" height="30"><span class="logo">I-</span><span
				class="logo1">fill </span></td>
			<td width="399" height="45" align="right" valign="top"><a href="#" id='logoutButton'><img src="${pageContext.request.contextPath}/images/logout.png" alt="logout.png" width="120" height="40" /></a></td>
		</tr>
	</table>
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td><img src="${pageContext.request.contextPath}/images/banner2.jpg" alt="image2" width="1003" height="150" /></td>
		</tr>
	</table>
	<br />
	<table width="945" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="263" height="40" align="left"
				style="background-image: url(${pageContext.request.contextPath}/images/report_button.png); background-repeat: no-repeat"
				class="subheading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manage Balances</td>
			<td width="740">&nbsp;</td>
		</tr>
	</table>
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column">
			<form method="POST" id="balances" action="/ifill/ifill/admin/postBalances">
				<table class="table table-condensed table-striped" border='0' align='center'>
					<tbody>
						<tr>
							<td><div class="col-md-offset-4"><label for='date' class='control-label'>Date<span color='red'>*</span> </label></div></td>
							<td><div class="col-md-offset-1 col-md-4"> <input type="text" id='date' name='date' class="form-control" data-date-format="dd-mm-yyyy"/></div></td>
						</tr>
						<tr>
							<td><div class="col-md-offset-4"><label for='yesterdays' class='control-label'>Previous  Balance</label></div></td>
							<td>
								<div class="col-md-offset-1 col-md-4"> 
									<input type="text" id='yesterdays' name='yesterdays' class="form-control" disabled='disabled'/>
									<input type='hidden' id='balanceYesterday' name='balanceYesterday' />
								</div>
							</td>
						</tr>
						<tr>
							<td><div class="col-md-offset-4"><label for='sales' class='control-label'>Current Cash Position </label></div></td>
							<td>
								<div class="col-md-offset-1 col-md-4"> 
									<input type="text" id='sales' name='sales' class="form-control" disabled='disabled'/>
									<input type="hidden" id='sale' name='sale' class="form-control"/>
								</div>
							</td>
						</tr>
						<tr>
							<td><div class="col-md-offset-4"><label for='rsDeposited' class='control-label'>Rs Deposited in Bank<span color='red'>*</span></label></div></td>
							<td><div class="col-md-offset-1 col-md-4"> <input type="text" id='rsDeposited' name='rsDeposited' class="form-control" /></div></td>
						</tr>
						<tr>
							<td><div class="col-md-offset-4"><label for='rsTransferred' class='control-label'>Rs Transfered to Company<span color='red'>*</span> </label></div></td>
							<td><div class="col-md-offset-1 col-md-4"> <input type="text" id='rsTransferred' name='rsTransferred' class="form-control" /></div></td>
						</tr>
						<tr>
							<td><div class="col-md-offset-4"><label for='newAddition' class='control-label'>New Amount to Manager</label></div></td>
							<td><div class="col-md-offset-1 col-md-4"> <input type="text" id='newAddition' name='newAddition' class="form-control" /></div></td>
						</tr>
						<tr>
							<td><div class="col-md-offset-4"><label for='available' class='control-label'>Current Balance </label></div></td>
							<td><div class="col-md-offset-1 col-md-4"> 
							<input type="text" id='available' name='available' class="form-control" disabled='disabled'/>
							<input type="hidden" id='availableBal' name='availableBal' class="form-control"/></div></td>
						</tr>
					</tbody>
				</table>
				<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='submit_row' class="pull-right btn btn-default">&nbsp;&nbsp;&nbsp; Submit &nbsp;&nbsp;&nbsp;</a></td></tr></table>
			</form>
			</div>
			
		</div>
		<hr width="75%">
	</div>
	
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-offset-1 col-md-10 column" >
			<table class="table table-condensed table-striped table-bordered " id="tab_logic">
				<thead>
				<tr>
					<th class="text-center"></th>
					<th class="text-center">Date</th>
					<th class="text-center">Yesterday's</th>
					<th class="text-center">Sale</th>
					<th class="text-center">Deopsited Today</th>
					<th class="text-center">Transfered Company</th>
					<th class="text-center">Amount to Manager</th>
					<th class="text-center">Available Balance</th>
				</tr>
				</thead>
  				<tr id="readingsGrid1"></tr>
			</table>
			<table ><tr><td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td class="col-md-offset-1 col-md-10"></td>
						<td><a id='backbutton' href="/ifill/ifill/views/showAdminHome" class="pull-right btn btn-default">Go Back</a></td></table>
			</div>
		</div>
	</div>
			
	<br />
	<br />
	<br />	
	<br />
	<br />
	<br />
		<br />
	<table width="1003" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td height="30" align="center" bgcolor="#8780b0" class="copyright">@copyright
				2013 . All Rights Reserved</td>
		</tr>
	</table>
	<div id="errorModal" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title"><img width="35" height="35" alt="" src="${pageContext.request.contextPath}/images/error-icon.png">&nbsp;&nbsp;Error Alert !! </h4>
                </div>
                <div class="modal-body">
                	<p id='errorMsg'></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>