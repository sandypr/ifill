<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ifill : Daily Sales</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/skitter.css" type="text/css">
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript">
	
	$(document)
			.ready(
					function() {
					
					$.ajax({
							type : "GET",
							url : "/ifill/ifill/views/orgDetails",
							contentType : 'application/json',
							dataType : "text",
							success : function(ldr) {
								var finalData = jQuery.parseJSON(ldr);
								console.log(finalData['name']);
								$('#orgName').append("<h3> "+finalData['name']+"</h3><h4>"+finalData['address']+"</h4>");
							},
							error : function(xhr) {
								alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "
										+ xhr.status);
							}
						});
						
					$.ajax({
						type : "GET",
						url : "/ifill/ifill/reports/showPeriodicVariationData",
						contentType : 'application/json',
						dataType : "text",
						success : function(respWrapper) {												
							console.log(respWrapper);
							populateData(respWrapper);
						},
						error : function(xhr) {
							alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "
									+ xhr.status);
						}
					});	
					
					function populateData(respWrapper){
						var dipSalesSummary = {};
						var meterSalesSummary = {};
						var meterSalesTotals = {};
						
						var finalData = jQuery.parseJSON(respWrapper);
						
						var dipsData = finalData.reports;
						console.log(dipsData);
						
						// get key strings from dipsData
						var keys = dipsData.keys;
						var keyStrings = keys.split(",");
						
						// pull data for dips using keys
						$.each(keyStrings, function(index,keyString){
							console.log(keyString);
							console.log(index);
							
							// prepare keys
							var opening = keyString+"_FROM_DIP";
							var stock = keyString+"_TOTAL_STOCK";
							var closing = keyString+"_TO_DIP";
							
							// pull out the data
							var openingDip = dipsData[opening];
							var stockRecieved = dipsData[stock];
							var closingDip = dipsData[closing];
							
							$('#dip_details'+index).append("<td class='text-center'><div class='col-md-offset-1 col-md-10'><strong>"+keyString+"</strong></div></td>");
							$('#dip_details'+index).append("<td class='text-right'><div class='col-md-offset-1 col-md-10'>"+openingDip+"</div></td>");
							$('#dip_details'+index).append("<td class='text-right'><div class='col-md-offset-1 col-md-10'>"+stockRecieved+"</div></td>");
							$('#dip_details'+index).append("<td class='text-right'><div class='col-md-offset-1 col-md-10'>"+closingDip+"</div></td>");
							
							var totalDipSale = (Number(openingDip) + Number(stockRecieved)) - Number(closingDip) ;
							console.log(totalDipSale);
							$('#dip_details'+index).append("<td class='text-center'><div class='col-md-offset-1 col-md-10'><strong>"+totalDipSale+"</strong></div></td>");
							
							// add it to summary map for further reference
							dipSalesSummary[keyString] = totalDipSale;
							
							// add place holder for next row if any
							var nextIndex = index+1;
							$('#dips').append("<tr id='dip_details"+nextIndex+"' ></tr>");
						});
						
						// meter sales details
						var meterSalesMap = {};
						
						$.each(finalData.reportRespList, function(index, value) {
							//console.log(value);
							//pumpWise_meterSales0
														
							// pull data for dips using keys
							$.each(keyStrings, function(index,keyString){
								if(keyString == value.product){
									var row = "";
									if(meterSalesMap[keyString] == null){
										// new fuel
										row = row + ("<tr><td class='text-center'><div class='col-md-offset-1 col-md-10'><strong>"+keyString+"</strong></div></td>");
										row = row + ("<td><div class='col-md-offset-1 col-md-10'>&nbsp;&nbsp;&nbsp;</div></td>");
										row = row + ("<td><div class='col-md-offset-1 col-md-10'>&nbsp;&nbsp;&nbsp;</div></td>");
										row = row + ("<td><div class='col-md-offset-1 col-md-10'>&nbsp;&nbsp;&nbsp;</div></td>");
										row = row + ("<td><div class='col-md-offset-1 col-md-10'>&nbsp;&nbsp;&nbsp;</div></td>");
										row = row + ("<td><div class='col-md-offset-1 col-md-10'>&nbsp;&nbsp;&nbsp;</div></td></tr>");
										meterSalesTotals[keyString] = 0;
										meterSalesMap[keyString] = "";
										
										//alert(row);
									}
									// not new, so add a row
									var sales = Number(value.closing) - Number(value.opening);
									row = row + ("<tr>");
									row = row + ("<td><div class='col-md-offset-1 col-md-10'>&nbsp;&nbsp;&nbsp;</div></td>");
									row = row + ("<td class='text-center'><div class='col-md-offset-1 col-md-10'>"+value.pumpId+"</div></td>");
									row = row + ("<td class='text-right'><div class='col-md-offset-1 col-md-10'>"+value.opening+"</div></td>");
									row = row + ("<td class='text-right'><div class='col-md-offset-1 col-md-10'>"+value.closing+"</div></td>");
									row = row + ("<td class='text-right'><div class='col-md-offset-1 col-md-10'>"+sales+"</div></td>");
									row = row + ("<td><div class='col-md-offset-1 col-md-10'>&nbsp;&nbsp;&nbsp; </div></td>");
									row = row + ("</tr>");
									
									meterSalesTotals[keyString] = Number(meterSalesTotals[keyString]) + (Number(value.closing)-Number(value.opening));
									
									meterSalesMap[keyString] = meterSalesMap[keyString] + (row);						
									console.log(row);
								}
							});
						});
						
						console.log(meterSalesMap);
						console.log(meterSalesTotals);
						
						$.each(keyStrings, function(index,keyString){
							
							$('#meterReadings').append(meterSalesMap[keyString]);
							
							var summaryRow = "";
							summaryRow = summaryRow + "<tr>";
							summaryRow = summaryRow + "<td class='text-right'><div class='col-md-offset-1 col-md-10'></div></td>";
							summaryRow = summaryRow + "<td class='text-right'><div class='col-md-offset-1 col-md-10'></div></td>";
							summaryRow = summaryRow + "<td class='text-right'><div class='col-md-offset-1 col-md-10'></div></td>";
							summaryRow = summaryRow + "<td class='text-right'><div class='col-md-offset-1 col-md-10'></div></td>";
							summaryRow = summaryRow + "<td class='text-right'><div class='col-md-offset-1 col-md-10'></div></td>";
							summaryRow = summaryRow + "<td class='text-center'><div class='col-md-offset-1 col-md-10'><strong>"+Number(meterSalesTotals[keyString])+"</strong> </div></td>";
							summaryRow = summaryRow + "<tr>";
							
							$('#meterReadings').append(summaryRow);
							
						});
						
						// variation summary
						$.each(keyStrings, function(index,keyString){
						
							var variation = Number(meterSalesTotals[keyString]) - Number(dipSalesSummary[keyString]);
							var row = "";
							row = row + ("<tr>");
							row = row + ("<td class='text-center'><div class='col-md-offset-1 col-md-10'>"+(index+1)+"</div></td>");
							row = row + ("<td class='text-center'><div class='col-md-offset-1 col-md-10'>"+keyString+"</div></td>");
							row = row + ("<td class='text-center'><div class='col-md-offset-1 col-md-10'>"+dipSalesSummary[keyString]+"</div></td>");
							row = row + ("<td class='text-center'><div class='col-md-offset-1 col-md-10'>"+meterSalesTotals[keyString]+"</div></td>");
							row = row + ("<td class='text-center'><div class='col-md-offset-1 col-md-10'><strong>"+variation+"</strong></div></td>");
							row = row + ("</tr>");
							
							$('#stockRows').append(row);
						});
					}
					
							
		$('#logoutButton').click(function(e){
			$.ajax({
				type : "GET",
				url : "/ifill/ifill/profile/logout",
				contentType : 'application/json',
				dataType : "text",
				success : function(status) {												
					//alert('successfully logged out'+status);
					window.location.href = "/ifill/ifill/views/showAppHomePage";
				},
				error : function(xhr) {
					alert("Opps !! We could not delete the row now. Please try again. Please find the cause : "
							+ xhr.status);
				}
			});
		});
					
					});
	
</script>
<style type="text/css">
.Text_grey {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: normal;
	color: #999999;
}
</style>
</head>

<body>
<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="604" height="30"><span class="logo">I-</span><span class="logo1">fill </span></td>
    <td width="399" height="45" align="right" valign="top"><a href="#" id='logoutButton'><img src="${pageContext.request.contextPath}/images/logout.png" alt="logout.png" width="120" height="40" /></a></td>
  </tr>
</table>

<br />
<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="252" height="40" align="left" valign="middle" class="subheading" style="background-image:url(${pageContext.request.contextPath}/images/report_button.png); background-repeat:no-repeat">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Periodic Stock</td>
    <td width="751"><br />
   </td>
  </tr>
</table>
<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="887">
    <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #cccccc;
    border-radius:5px; background-image:url(${pageContext.request.contextPath}/images/login_bg.jpg); background-repeat:repeat" bgcolor="#f7f7f7">
      <tr>
        <td height="42" align="center" valign="middle"></br>
          <table>
          		<tr align="center"><td id='orgName'></td></tr>
				<!-- <tr align="center"><td><h3>ABC Filling Station, </br></h3><h4>PQR Road, XYZ Dist.</h4></td></tr> -->
				<!-- <tr align="center"><td><h4>Tanikella Road, Khammam Dist.</h4></td></tr> -->							
			</table> 
			<table width="70%" border="0" align="center" cellpadding="0"
				cellspacing="0">
				<tr align="left">
					<td width="418"></td>
					<td width="562" height="25" align="right" valign="bottom">
							&nbsp;&nbsp;<span class="para5">Report Period : <strong><%= session.getAttribute("fromDate").toString() %> </strong>&nbsp;&nbsp;to&nbsp;&nbsp; <strong><%= session.getAttribute("toDate").toString() %></strong></span>
						</td>
				</tr>
			</table><hr width="75%" align="center" /></br>   
			<div class="col-md-offset-1 col-md-10 column" >
            <table width="85%" border="0" align="center" id='dips' class="table table-striped table-bordered table-condensed " cellpadding="2" cellspacing="1">
            	<thead>
	              <tr>
	                <th height="30" class="text-left" bgcolor="#fbd073"><strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size='3'><u></u></font></strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> Opening Dip</strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> </strong><strong>Stock Reciept</strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> </strong><strong>Closing Dip</strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> Sales </strong></th>
	              </tr>
              </thead>
              <tr id='dip_details0'> </tr>
             
            </table>
            </div>             
            <div class="col-md-offset-1 col-md-10 column" >
            <table width="85%" border="0" align="center" class="table table-striped table-bordered table-condensed " cellpadding="2" cellspacing="1" id='meterReadings'>
            	<thead>
	              <tr>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> Product </strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> Pump </strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> Opening </strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> Closing </strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong>Sale</strong></th>
	                <th height="30" class="text-center" bgcolor="#fbd073"><strong> Total Sales </strong></th>
	              </tr>
              </thead>
              <tr id='pumpWise_meterSales0'> </tr>
            </table>
            </div>
            <br />
            <br />

            <div class="col-md-offset-1 col-md-10 column" >
            <table width="85%" cellpadding="2" cellspacing="1" class="table table-striped table-bordered table-condensed " id="stockRows">
				<thead>
				<tr>
					<th class="text-center" bgcolor="#aeb1e0" align="center">S.No</th>
					<th class="text-center" bgcolor="#aeb1e0" align="center">Product</th>
					<th class="text-center" bgcolor="#aeb1e0" align="center">Dip Sales</th>
					<th class="text-center" bgcolor="#aeb1e0" align="center">Meter Sales</th>
					<th class="text-center" bgcolor="#aeb1e0" align="center">Variation</th>
				</tr>					
				</thead>
				<tr id='variation_details'></tr>
  				
			</table>
			</div>
          <br />

          <div class="col-md-offset-1 col-md-10 column" >
          <!-- <table >
          	<tr>
          	<td class="col-md-offset-1 col-md-8"></td>
			<td class="col-md-offset-1 col-md-8"></td>
			<td class="col-md-offset-1 col-md-8"></td>
			<td><a id='backbutton' href="/ifill/ifill/inventory/showHomePage" class="pull-left btn btn-default">Go Back</a></td>
			</tr>
		</table> -->
		<br />
		<div class="col-md-offset-1 col-md-10 column" id="test">
							<table ><tr><td align="left"></td>
								<td align="right"></td>
								<td  class="col-md-offset-1 col-md-10" ></td>
								<td  class="col-md-offset-1 col-md-10" ><a id='saveButton' href="#" class="pull-left btn btn-success" onclick="divPrint();">Print / Save</a></td>
								<td><a id='backbutton' href="/ifill/ifill/inventory/showHomePage" class="pull-left btn btn-warning">Go Back</a></td>
								</table>
								<script type="text/javascript">
							      function divPrint() {
							      	//alert("printing");
							        //$("#print_div").addClass("printable");
							        //$('body').css('display','none');
							        
							        //$('#print_div').css('display', 'block');
							        //alert('added printable');
							        $('#logout_button').css('display','none');
							        $('#saveButton').css('display','none');
							        $('#backbutton').css('display','none');
							        window.print();
							      }
							    </script>
						</div>
						<br /><br /><br /><br />
		</div>
          <br />
          <br /><p></p>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<br />
<table width="1003" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" align="center" bgcolor="#8780b0" class="copyright">@copyright 2013 . All Rights Reserved</td>
  </tr>
</table>
</body>
</html>
