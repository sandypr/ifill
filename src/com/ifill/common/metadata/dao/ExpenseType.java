package com.ifill.common.metadata.dao;

public class ExpenseType {

	private String expType;
	private String expName;
	public String getExpType() {
		return expType;
	}
	public void setExpType(String expType) {
		this.expType = expType;
	}
	public String getExpName() {
		return expName;
	}
	public void setExpName(String expName) {
		this.expName = expName;
	}
}
