/**
 * 
 */
package com.ifill.common.metadata.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.ifill.common.exceptions.DataAccessLayerException;
import com.ifill.common.utils.IfillConstants;


/**
 * @author Sandy
 *
 */
@Service
public class MetadataDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String NO_OF_PUMPS = "SELECT count(*) no_of_pumps FROM pumps WHERE product_id = ? AND status = 'ACTIVE'";
	private static String FIND_PUMPS_BY_PRODUCT = "SELECT pump_id, name FROM pumps WHERE product_id = ? AND status = 'ACTIVE'";
	private static String FUEL_PRICE = "SELECT invoice_price, unit_price from products where product_id = ?";
	private static String FIND_ALL_LUBES = "SELECT product_id FROM products WHERE product_type = 'LUBE'";
	private static String FIND_ALL_FUELS = "SELECT product_id FROM products WHERE product_type in ('FUEL')";
	private static String FIND_ALL_PRODUCTS = "SELECT product_id,product_type, name, unit_price FROM products";
	private static String FIND_ALL_PRODUCT_TYPES = "SELECT product_type_code FROM product_types";
	private static String FIND_EXPENSE_TYPES = "SELECT type, description from expense_types";
	
	public int findPumpsByType(long type){
		
		List<Map<String,Object>> result = jdbcTemplate.queryForList(NO_OF_PUMPS,type);
		return (int) result.get(0).get(IfillConstants.CONSTANTS_NO_OF_PUMPS);
	}
	
	public List<String> findProductTypes(){
		
		List<String> types = new ArrayList<>();
		try{
			List<Map<String, Object>> result = jdbcTemplate.queryForList(FIND_ALL_PRODUCT_TYPES);
			
			for(Map<String,Object> row : result){
				types.add(row.get(IfillConstants.CONSTANTS_PRODUCT_TYPE_CODE).toString());
			}
			
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		return types;
	}
	
	public List<Map<String, String>> findExpenseTpyes(){
		
		List<Map<String,String>> typeList = new ArrayList<>();
		List<Map<String,Object>> result = jdbcTemplate.queryForList(FIND_EXPENSE_TYPES);
		for(Map<String,Object> row : result){
			Map<String,String> types = new HashMap<>();
			types.put("Type",row.get("type").toString());
			types.put("Desc",row.get("description").toString());
			
			typeList.add(types);
		}
		return typeList;
	}
	
	public List<Map<String, String>> findPumpsByFuel(String pid){
		
		List<Map<String,String>> pumpList = new ArrayList<>();
		List<Map<String,Object>> result = jdbcTemplate.queryForList(FIND_PUMPS_BY_PRODUCT,pid);
		for(Map<String,Object> row : result){
			Map<String,String> pumps = new HashMap<>();
			pumps.put(IfillConstants.CONSTANTS_PUMP_ID,row.get(IfillConstants.CONSTANTS_PUMP_ID).toString());
			pumps.put(IfillConstants.CONSTANTS_NAME,row.get(IfillConstants.CONSTANTS_NAME).toString());
			
			pumpList.add(pumps);
		}
		return pumpList;
	}
	
	public Map<String, Double> findPriceById(String id){
		List<Map<String, Double>> prices = null;
		Object[] params = {id};
		int[] types = {Types.VARCHAR};
		try{
			prices = jdbcTemplate.query(FUEL_PRICE,params,types,  new RowMapper<Map<String, Double>>() {
			      public Map<String, Double> mapRow(ResultSet resultSet, int i) throws SQLException {
			    	  Map<String,Double> map = new HashMap<>();
			    	  map.put("UnitPrice", resultSet.getDouble(2));
			    	  map.put("InvoicePrice", resultSet.getDouble(1));
			          return map;
			        }
			      });
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		return prices.get(0);
	}
	
	public List<String> pullLubes(){
		List<String> lubes = new ArrayList<>();
		List<Map<String,Object>> result = jdbcTemplate.queryForList(FIND_ALL_LUBES);
		for(Map<String,Object> row : result){
			lubes.add(row.get(IfillConstants.CONSTANTS_PRODUCT_ID).toString());
		}
		return lubes;
	}
	
	public List<String> findAllFuels(){
		List<String> fuels = new ArrayList<>();
		try{
			List<Map<String,Object>> result = jdbcTemplate.queryForList(FIND_ALL_FUELS);
			for(Map<String,Object> row : result){
				fuels.add(row.get(IfillConstants.CONSTANTS_PRODUCT_ID).toString());
			}
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		return fuels;
	}
	
	public List<Item> findAllProducts(){
		List<Item> products = new ArrayList<>();
		try{
			List<Map<String,Object>> result = jdbcTemplate.queryForList(FIND_ALL_PRODUCTS);
			for(Map<String,Object> row : result){
				Item item = new Item();
				item.setProductId(row.get(IfillConstants.CONSTANTS_PRODUCT_ID).toString());
				item.setProductType(row.get(IfillConstants.CONSTANTS_PRODUCT_TYPE).toString());
				item.setName(row.get(IfillConstants.CONSTANTS_NAME).toString());
				//item.setDesc(row.get(IfillConstants.CONSTANTS_DESC).toString());
				item.setUnitPrice(Double.parseDouble(row.get(IfillConstants.CONSTANTS_UNIT_PRICE).toString()));		
				
				products.add(item);
			}
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		return products;
	}
	
}

