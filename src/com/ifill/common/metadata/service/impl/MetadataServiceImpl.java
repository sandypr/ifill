/**
 * 
 */
package com.ifill.common.metadata.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifill.common.exceptions.DataAccessLayerException;
import com.ifill.common.exceptions.ServiceLayerException;
import com.ifill.common.metadata.dao.Item;
import com.ifill.common.metadata.dao.MetadataDao;
import com.ifill.common.metadata.service.MetadataServiceI;

/**
 * @author Sandy
 *
 */
@Service
public class MetadataServiceImpl implements MetadataServiceI{
	
	@Autowired
	MetadataDao metadataDao;

	@Override
	public Map<String, Double> findPriceById(String id) {
		Map<String, Double> price = null;
		try{
			price  = metadataDao.findPriceById(id);
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
		return price;
	}
	
	@Override
	public List<Map<String, String>> findExpenseTypes() {
		List<Map<String, String>> price = null;
		try{
			price  = metadataDao.findExpenseTpyes();
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
		return price;
	}
	
	@Override
	public List<Map<String, String>> findPumpsByFuel(String product_id){
		try{
			return metadataDao.findPumpsByFuel(product_id);
		}catch(DataAccessLayerException ex){
			ex.printStackTrace();
			throw new ServiceLayerException(ex.getMessage());
		}	
	}
	
	@Override
	public List<String> findAllLubes(){
		try{
			return metadataDao.pullLubes();
		}catch(DataAccessLayerException ex){
			ex.printStackTrace();
			throw new ServiceLayerException(ex.getMessage());
		}	
	}

	@Override
	public List<String> findAllFuels() {
		try{
			return metadataDao.findAllFuels();
		}catch(DataAccessLayerException ex){
			ex.printStackTrace();
			throw new ServiceLayerException(ex.getMessage());
		}
	}
	
	@Override
	public List<String> findAllProductTypes() {
		try{
			return metadataDao.findProductTypes();
		}catch(DataAccessLayerException ex){
			ex.printStackTrace();
			throw new ServiceLayerException(ex.getMessage());
		}
	}
	
	@Override
	public List<Item> findAllProducts() {
		try{
			return metadataDao.findAllProducts();
		}catch(DataAccessLayerException ex){
			ex.printStackTrace();
			throw new ServiceLayerException(ex.getMessage());
		}
	}
}
