/**
 * 
 */
package com.ifill.common.metadata.service;

import java.util.List;
import java.util.Map;

import com.ifill.common.metadata.dao.Item;

/**
 * @author Sandy
 *
 */
public interface MetadataServiceI {
	public Map<String, Double> findPriceById(String id);

	List<Map<String, String>> findPumpsByFuel(String product_id);
	List<String> findAllLubes();
	List<String> findAllFuels();
	List<String> findAllProductTypes();
	List<Item> findAllProducts();
	List<Map<String, String>> findExpenseTypes();
}
