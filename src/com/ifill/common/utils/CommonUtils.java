/**
 * 
 */
package com.ifill.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author Sandy
 *
 */
public class CommonUtils {

	private static DateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
	
	public static Date getCurrentDate(){	
		Date today = Calendar.getInstance().getTime(); 
		return today;
	}
	
	public static String getCurrentDateAsString(){	
		Date today = Calendar.getInstance().getTime(); 
		return dateformat.format(today);
	}
	
	public static String getFormattedDateAsString(Date date){
		if(date != null)
			return dateformat.format(date);
		return null;
	}
	
	public static Date getDate(String str){
		if(str != null)
			try {
				return dateformat.parse(str);
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		return null;
	}
	
	public static Date getCustomDate(String format,String str){
		if(str != null)
			try {
				DateFormat dateformat = new SimpleDateFormat(format);
				return dateformat.parse(str);
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		return null;
	}
	
	public static int convertMonthToDigit(String month) throws ParseException{
		Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    int mnth = cal.get(Calendar.MONTH);
	    return mnth;
	}
	
}
