/**
 * 
 */
package com.ifill.common.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Sandy
 *
 */
public class VirtualProxy {

	private static Properties properties;
	private static VirtualProxy instance;
	private VirtualProxy(){
	}
	
	public static VirtualProxy getInstance(){
		if(instance == null){
			instance = new VirtualProxy();
			
			loadProps();
		}
		return instance;
	}
	
	private static void loadProps(){
		properties = new Properties();
		InputStream input = null;
		
		try {
			 
			input = new FileInputStream("ifill.properties");
	 
			// load a properties file
			properties.load(input);
	 
			// get the property value and print it out
			System.out.println(properties.getProperty("firm.name"));
			System.out.println(properties.getProperty("firm.address"));
			System.out.println(properties.getProperty("date.backward.days"));
	 
		} catch (IOException ex) {
			//ex.printStackTrace();
			try {
				properties.load(VirtualProxy.class.getClassLoader().getResourceAsStream("ifill.properties"));
				System.out.println(properties.getProperty("firm.name"));
				System.out.println(properties.getProperty("firm.address"));
				System.out.println(properties.getProperty("date.backward.days"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public String getValue(String key){
		return properties.getProperty(key);
	}
	
}
