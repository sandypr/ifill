/**
 * 
 */
package com.ifill.common.utils;

/**
 * @author Sandy
 *
 */
public interface IfillConstants {
	
	static String SESSION_ISADMIN = "isAdmin";
	static String SESSION_LOGGEDIN_STATUS = "loggedIn";
	static String SESSION_TIMED_OUT = "Session Timedout";
	static int STATUS_UNAUTHORIZED_CODE = -1;
	static int STATUS_FAILURE_CODE = -99;
	static String STATUS_SUCCESS = "Success";
	static String STATUS_FAILURE = "Failure";
	static String STATUS_UNAUTHORIZED = "UnAuthorized";
	
	static String CONSTANTS_PRODUCT = "product";
	static String CONSTANTS_PRODUCT_ID = "product_id";
	static String CONSTANTS_PRODUCT_TYPE = "product_type";
	static String CONSTANTS_NAME = "name";
	static String CONSTANTS_DESC = "desc";
	static String CONSTANTS_INVOICE_PRICE = "invoice_price";
	static String CONSTANTS_PUMP_ID = "pump_id";
	static String CONSTANTS_NO_OF_PUMPS = "no_of_pumps";
	static String CONSTANTS_UNIT_PRICE = "unit_price";
	static String CONSTANTS_REORDER_LEVEL = "reorder_level";
	static String CONSTANTS_REORDER_QUANTITY = "reorder_quantity";
	static String CONSTANTS_AVAILABLE_QUANTITY = "available_quantity";
	static String CONSTANTS_PRODUCT_TYPE_CODE = "product_type_code";
	static String CONSTANTS_ENAME = "ename";
	static String CONSTANTS_SALE = "sale";
	static String CONSTANTS_QUANTITY = "quantity";
	static String CONSTANTS_FUEL = "fuel";
	static String CONSTANTS_DATE = "date";
	static String CONSTANTS_DIPS = "dip";
	static String CONSTANTS_STOCK = "stock";
	static String CONSTANTS_READING_IN_METERS = "reading_in_liters";
	
	static String CONSTANTS_ACTIVE = "ACTIVE";
	static String CONSTANTS_INACTIVE = "INACTIVE";
	
	static String CONSTANTS_MS = "MS";
	static String CONSTANTS_HSD = "HSD";
	
	static String CONSTANTS_KEY_SEPERATOR = "~";
	//static String CONSTANTS_ = "";

}
