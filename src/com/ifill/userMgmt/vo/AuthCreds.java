/**
 * 
 */
package com.ifill.userMgmt.vo;

/**
 * @author Sandy
 *
 */
public class AuthCreds {
	
	private String userId;
	private String password;
	private String isAdmin;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}
}
