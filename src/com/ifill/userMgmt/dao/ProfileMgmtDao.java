/**
 * 
 */
package com.ifill.userMgmt.dao;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.ifill.common.exceptions.DataAccessLayerException;
import com.ifill.common.utils.CommonUtils;
import com.ifill.common.utils.IfillConstants;
import com.ifill.userMgmt.vo.Customer;
import com.ifill.userMgmt.vo.User;

/**
 * @author Sandy
 *
 */
@Service
public class ProfileMgmtDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String ROLES_ADMIN = "ADMIN";
	private static String ROLES_SUPERVISOR = "SUPERVISOR";
	
	private static String FIND_USER_BY_LOGIN_ID = "SELECT ename from users where login_id = ? AND password = ? AND role_id = ?";
	private static String CREATE_USER = "INSERT INTO users(ename, bloodgroup, login_id, password, role_id, status) VALUES(?,?,?,?,?,?)";
	private static String DELETE_USER = "DELETE FROM users where emp_id = ?";
	private static String UPDATE_USER_CREDS = "UPDATE users SET login_id = ?, password = ? WHERE emp_id = ?";
	private static String CREATE_CREDIT_CUSTOMER = "INSERT INTO credit_customers(customer_name,address,contact_no,balance,active) VALUES (?, ?, ?, ?, ?)";
	private static String CREATE_CREDITBAL_SUMMARY = "INSERT INTO credit_balances(customer_id, summarized_date, initial_balance) VALUES (?, ?, ?)";
	private static String DELETE_CREDIT_CUST = "DELETE FROM credit_customers where customer_name = ?";
	private static String DELETE_CREDIT_CUST_BALANCES = "DELETE FROM credit_balances where customer_id = (select credit_customer_id from credit_customers where customer_name = ?)";
	private static String GET_LATEST_EMPID = "SELECT IFNULL(max(emp_id),0) as id FROM users";
	private static String GET_LATEST_CREDIT_CUSTOMERID = "SELECT IFNULL(max(credit_customer_id),0) as id FROM credit_customers";
	private static String GET_CCUSTOMERS_WITH_CHARS = "SELECT credit_customer_id,customer_name from credit_customers";
	
	public String findUserByLoginId(String login, String password, boolean isAdmin){
		Map<String,Object> rows = null;
		try{
			rows = jdbcTemplate.queryForMap(FIND_USER_BY_LOGIN_ID, new Object[]{login,password, (isAdmin)? ROLES_ADMIN : ROLES_SUPERVISOR});
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		if(rows.isEmpty() || rows.size()>1){
			return null;
		}else{
			return rows.get(IfillConstants.CONSTANTS_ENAME).toString();
		}
	}
	
	public void createUser(User user){
		Object[] params = {user.getName(), user.getBloodgroup(), user.getUserId(), user.getPassword(), user.getRole(), IfillConstants.CONSTANTS_ACTIVE};
		int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,  Types.VARCHAR};
		try{
			jdbcTemplate.update(CREATE_USER, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	// delete user
	public void deleteUser(String key){
		
		String[] compoundKeys = key.split(IfillConstants.CONSTANTS_KEY_SEPERATOR);
		Object[] params = {compoundKeys[0]};
		int[] types = {Types.VARCHAR};
		
		try{
			jdbcTemplate.update(DELETE_USER, params, types);
		}catch(DataAccessException dae){
			//TODO
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public void createUserLoginCreds(User user){
		Object[] params = {user.getUserId(), user.getPassword(),user.getEmpId()};
		int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		try{
			jdbcTemplate.update(UPDATE_USER_CREDS, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	@Transactional
	public void createCreditUser(Customer user){
		
		try{
			Integer result = 0;
			result = jdbcTemplate.update(CREATE_CREDIT_CUSTOMER, 
					new Object[]{user.getName(), user.getAddress(), user.getMobile(), user.getInitialBalance(), 1}, 
					new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.DOUBLE, Types.TINYINT});
			System.out.println(result);
			if(result > 0){
				Integer latestId = 0;
				latestId = jdbcTemplate.queryForObject(GET_LATEST_CREDIT_CUSTOMERID, Integer.class);
				jdbcTemplate.update(CREATE_CREDITBAL_SUMMARY, 
						new Object[]{latestId,CommonUtils.getCurrentDate(),user.getInitialBalance()}, 
						new int[]{Types.INTEGER, Types.DATE, Types.DOUBLE});
			}
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	// delete credit customer
	public void deleteCreditCustomer(String key){
		String[] compoundKeys = key.split(IfillConstants.CONSTANTS_KEY_SEPERATOR);
		Object[] params = {compoundKeys[0]};
		int[] types = {Types.VARCHAR};
		
		try{
			jdbcTemplate.update(DELETE_CREDIT_CUST_BALANCES, params, types);
			jdbcTemplate.update(DELETE_CREDIT_CUST, params, types);
		}catch(DataAccessException dae){
			//TODO
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public List<Map<String, Object>> findCreditCustomers(){
		
		List<Map<String,Object>> rows = null;
		try{
			rows = jdbcTemplate.queryForList(GET_CCUSTOMERS_WITH_CHARS);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		List<Map<String,Object>> list = null;
		for(Map<String, Object> row : rows){
			
		}
		return rows;
	}
	
public boolean isCreditCustomersUnique(String name){
		
		List<Map<String,Object>> rows = null;
		try{
			rows = jdbcTemplate.queryForList(GET_CCUSTOMERS_WITH_CHARS, new Object[]{name});
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		if(rows.isEmpty())
			return true;
		return false;
	}
	
	
}
