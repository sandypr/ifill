package com.ifill.userMgmt.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifill.common.exceptions.DataAccessLayerException;
import com.ifill.common.exceptions.ServiceLayerException;
import com.ifill.common.utils.IfillConstants;
import com.ifill.userMgmt.dao.ProfileMgmtDao;
import com.ifill.userMgmt.service.ProfileMgmtI;
import com.ifill.userMgmt.vo.AuthCreds;
import com.ifill.userMgmt.vo.Customer;
import com.ifill.userMgmt.vo.User;

@Service
public class ProfileMgmtServiceImpl implements ProfileMgmtI{
	
	@Autowired
	ProfileMgmtDao profileDao;
	
	@Override
	public String findUserById(AuthCreds user){
		
		try{
			return profileDao.findUserByLoginId(user.getUserId(), user.getPassword(), Boolean.parseBoolean(user.getIsAdmin()));
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
	
	}

	@Override
	public boolean isUserLoggedIn(HttpSession session) {
		
		boolean loggedIn = false;
		Object status = session.getAttribute(IfillConstants.SESSION_LOGGEDIN_STATUS);
		if(status != null){
			loggedIn = Boolean.valueOf(status.toString());
		}
		session.getMaxInactiveInterval();
		return loggedIn;
	}
	
	@Override
	public boolean isUserAuthorized(HttpSession session, boolean checkAdmin) {
		
		boolean authorized = false;
		
		try{
			Object status = session.getAttribute(IfillConstants.SESSION_LOGGEDIN_STATUS);
			boolean isAdmin = Boolean.parseBoolean(session.getAttribute(IfillConstants.SESSION_ISADMIN).toString());
			
			if(status != null){
				authorized = Boolean.valueOf(status.toString());
				if(checkAdmin)
					authorized = authorized && isAdmin;
			}
		}catch(Exception e){
			//e.printStackTrace();
			return authorized;
		}
		
		session.getMaxInactiveInterval();
		
		return authorized;
	}

	@Override
	public String createUser(User user) {
		
		try{
			profileDao.createUser(user);
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
		return IfillConstants.STATUS_SUCCESS;
	}
	
	@Override
	public String createUserCreds(User user) {
		
		try{
			profileDao.createUserLoginCreds(user);
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
		return IfillConstants.STATUS_SUCCESS;
	}
	
	@Override
	public String createCreditUser(Customer user) {
		try{
			profileDao.createCreditUser(user);
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
		return IfillConstants.STATUS_SUCCESS;
	}
	
	@Override
	public String deleteCreditUser(String key) {
		try{
			profileDao.deleteCreditCustomer(key);
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
		return IfillConstants.STATUS_SUCCESS;
	}
	
	@Override
	public List<Map<String, Object>> findCCNames(){
		
		try{
			return profileDao.findCreditCustomers();
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
	
	}
	
	@Override
	public boolean isCCNameUnique(String name){
		
		try{
			return profileDao.isCreditCustomersUnique(name);
		}catch(DataAccessLayerException dale){
			dale.printStackTrace();
			throw new ServiceLayerException(dale.getMessage());
		}
	
	}
}
