package com.ifill.userMgmt.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.ifill.userMgmt.vo.AuthCreds;
import com.ifill.userMgmt.vo.Customer;
import com.ifill.userMgmt.vo.User;

public interface ProfileMgmtI {

	boolean isUserLoggedIn(HttpSession session);
	String findUserById(AuthCreds user);
	String createUser(User user);
	String createUserCreds(User user);
	String createCreditUser(Customer user);
	String deleteCreditUser(String key);
	boolean isUserAuthorized(HttpSession session, boolean checkAdmin);
	List<Map<String, Object>> findCCNames();
	boolean isCCNameUnique(String name);
}
