/**
 * 
 */
package com.ifill.workbench.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ifill.common.utils.CommonUtils;
import com.ifill.common.utils.VirtualProxy;
import com.ifill.reports.vo.ReportsResponseWrapper;
import com.ifill.userMgmt.service.ProfileMgmtI;

/**
 * @author Sandy
 *
 */
@Controller
@RequestMapping("/views")
public class ViewController {
	
	private static String VIEWS_APP_HOME = "index";
	private static String VIEWS_SUPERVISOR_HOME = "WelcomePage";
	private static String VIEWS_ADMIN_HOME = "admin_ui/WelcomePage";
	private static String VIEWS_CONFIG_PUMP = "admin_ui/ConfigPump";
	private static String VIEWS_ADD_PRODUCT = "admin_ui/AddProduct";
	private static String VIEWS_CHANGE_PRICE = "admin_ui/ChangePrice";
	private static String VIEWS_CREATE_CREDITCUST = "admin_ui/AddNewCreditCustomer";
	private static String VIEWS_MANAGE_BALANCES = "admin_ui/ManageBalances";
	private static String VIEWS_ADD_USER = "admin_ui/AddUser";
	private static String VIEWS_INITIAL_READINGS = "admin_ui/InitialReadings";
	private static String VIEWS_DETAILED_CREDITS = "CreditSaleDetails";
	private static String VIEWS_ADD_EXPTYPE = "admin_ui/AddExpenseType";
	private static String VIEWS_EXP_REPORTS_PAGE = "admin_ui/ExpenseWiseReportPage";
	private static String VIEWS_ADMIN_REPORTS = "admin_ui/AdminReports";
	
	@Autowired
	private ProfileMgmtI profileMgmtService;

	@RequestMapping(method=RequestMethod.GET, value="/showAdminHome")
	public String showWelcomeView(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_ADMIN_HOME;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showInitialReadings")
	public String showInitialReadings(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_INITIAL_READINGS;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showAddProductView")
	public String showAddProductView(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_ADD_PRODUCT;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showChangePriceView")
	public String showChangePriceView(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_CHANGE_PRICE;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showPumpConfigView")
	public String showPumpConfigs(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_CONFIG_PUMP;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showAddUserView")
	public String showAddUser(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_ADD_USER;
		else
			return VIEWS_APP_HOME;
	}

	@RequestMapping(method=RequestMethod.GET, value="/showAddNewCreditCustomer")
	public String showAddNewCreditCustomer(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_CREATE_CREDITCUST;
		else
			return VIEWS_APP_HOME;
	}
	

	@RequestMapping(method=RequestMethod.GET, value="/showAddExpTypeView")
	public String showAddExpenseType(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_ADD_EXPTYPE;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showAdminReports")
	public String showAdminReports(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_ADMIN_REPORTS;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showExpenseReportsPage")
	public String showExpenseReportsPage(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true))
			return VIEWS_EXP_REPORTS_PAGE;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showHomePage")
	public String showHomeView(HttpServletRequest req){
		if(profileMgmtService.isUserLoggedIn(req.getSession()))
			return VIEWS_SUPERVISOR_HOME;
		else
			return VIEWS_APP_HOME;
			
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showAppHomePage")
	public String showAppHomeView(HttpServletRequest req){
		return VIEWS_APP_HOME;
	}
	
	
	@RequestMapping(method=RequestMethod.GET, value="/showDetailedCredits")
	public String showDetailedCreditView(HttpServletRequest req){
		System.out.println("asked for credit details");
		HttpSession session = req.getSession();

		if(session.getAttribute("ReportDate") == null){
			String dt = CommonUtils.getCurrentDateAsString();
			session.setAttribute("ReportDate", dt);
		}
		if(profileMgmtService.isUserLoggedIn(req.getSession()))
			return VIEWS_DETAILED_CREDITS;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showManageBalances")
	public String showManageBalancesView(HttpServletRequest req){
		if(profileMgmtService.isUserLoggedIn(req.getSession()))
			return VIEWS_MANAGE_BALANCES;
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/orgDetails")
	public @ResponseBody Map<String, String> showOrgDetails(HttpServletRequest req){
		Map<String,String> orgDetails = new HashMap<>();
		if(profileMgmtService.isUserLoggedIn(req.getSession())){
			VirtualProxy vProxy = VirtualProxy.getInstance();
			orgDetails.put("name", vProxy.getValue("firm.name"));
			orgDetails.put("address", vProxy.getValue("firm.address"));
		}
		return orgDetails;
	}
}
