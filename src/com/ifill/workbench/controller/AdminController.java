/**
 * 
 */
package com.ifill.workbench.controller;

import java.text.ParseException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ifill.common.metadata.dao.ExpenseType;
import com.ifill.common.metadata.dao.Item;
import com.ifill.common.utils.CommonUtils;
import com.ifill.common.utils.IfillConstants;
import com.ifill.reports.vo.ReportsResponseWrapper;
import com.ifill.userMgmt.service.ProfileMgmtI;
import com.ifill.userMgmt.vo.Customer;
import com.ifill.userMgmt.vo.User;
import com.ifill.workbench.service.AdminServiceI;
import com.ifill.workbench.vo.BalanceSummary;
import com.ifill.workbench.vo.Pumps;

/**
 * @author Sandy
 *
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private ProfileMgmtI profileMgmtService;
	
	@Autowired
	private AdminServiceI adminService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/createProduct", consumes = "application/json")
	public @ResponseBody String pushNewProduct(@RequestBody Item feed, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return adminService.addProduct(feed);	
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/changePrice", consumes = "application/json")
	public @ResponseBody String changePrice(@RequestBody Item feed, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserAuthorized(req.getSession(), true)){			
			return adminService.updatePrice(feed);
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{key}/deleteProduct", consumes = "application/json")
	public @ResponseBody String deleteProduct(@PathVariable String key, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return adminService.deleteProduct(key);
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{key}/deletePriceChange", consumes = "application/json")
	public @ResponseBody String deletePriceChange(@PathVariable String key, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return adminService.deletePriceChange(key);
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/createPump", consumes = "application/json")
	public @ResponseBody String createPump(@RequestBody Pumps feed, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return adminService.addPump(feed);	
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{key}/deletePump", consumes = "application/json")
	public @ResponseBody String deletePump(@PathVariable String key, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return adminService.deletePump(key);
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/createUser", consumes = "application/json")
	public @ResponseBody String pushNewUser(@RequestBody User user, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserAuthorized(req.getSession(), true)){			
			return profileMgmtService.createUser(user);	
		}else
			return IfillConstants.STATUS_UNAUTHORIZED;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/postUserCreds", consumes = "application/json")
	public @ResponseBody String pushUserCreds(@RequestBody User user, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return profileMgmtService.createUserCreds(user);	
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/createCreditCustomer", consumes = "application/json")
	public @ResponseBody String createCreditCustomer(@RequestBody Customer user, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return profileMgmtService.createCreditUser(user);
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "{key}/deleteCreditCustomer", consumes = "application/json")
	public @ResponseBody String deleteCreditCustomer(@PathVariable String key, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return profileMgmtService.deleteCreditUser(key);
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/balanceInits/{curdate}", consumes = "application/json")
	public @ResponseBody ReportsResponseWrapper getBalanceInits(@PathVariable("curdate") String curdate, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){	
			Date balanceDate = CommonUtils.getDate(curdate);
			return adminService.getPreviousBalances(balanceDate);
		}else
			return null;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/postBalances", consumes = "application/json")
	public @ResponseBody String pushBalances(@RequestBody BalanceSummary bal, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){	
			return adminService.updateBalances(bal);	
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/createExpenseType", consumes = "application/json")
	public @ResponseBody String createExpType(@RequestBody ExpenseType type, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){	
			System.out.println("i am posting expense type");
			return adminService.createExpenseType(type);	
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{key}/deleteExpenseType", consumes = "application/json")
	public @ResponseBody String deleteExpType(@PathVariable String key, HttpServletRequest req) throws ParseException{
		
		if(profileMgmtService.isUserLoggedIn(req.getSession())){			
			return adminService.deletePump(key);
		}else
			return IfillConstants.SESSION_TIMED_OUT;
	}
	
	
}
