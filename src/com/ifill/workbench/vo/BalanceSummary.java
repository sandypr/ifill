/**
 * 
 */
package com.ifill.workbench.vo;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.ifill.reports.vo.util.IFillDateSerializer;
import com.ifill.reports.vo.util.JsonDateDeserializer;

/**
 * @author Sandy
 *
 */
public class BalanceSummary {
	
	private Date date;
	private Double balanceYesterday;
	private Double sale;
	private Double rsDeposited;
	private Double rsTransferred;
	private Double availableBal;
	private Double newAddition;
	private Date stockDate;
	private Double invoiceCost;

	
	@JsonSerialize(using=IFillDateSerializer.class)
	public Date getDate() {
		return date;
	}
	
	@JsonDeserialize(using=JsonDateDeserializer.class)
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getBalanceYesterday() {
		return balanceYesterday;
	}
	public void setBalanceYesterday(Double balanceYesterday) {
		this.balanceYesterday = balanceYesterday;
	}
	public Double getSale() {
		return sale;
	}
	public void setSale(Double sale) {
		this.sale = sale;
	}
	public Double getRsDeposited() {
		return rsDeposited;
	}
	public void setRsDeposited(Double rsDeposited) {
		this.rsDeposited = rsDeposited;
	}
	public Double getRsTransferred() {
		return rsTransferred;
	}
	public void setRsTransferred(Double rsTransferred) {
		this.rsTransferred = rsTransferred;
	}
	public Double getAvailableBal() {
		return availableBal;
	}
	public void setAvailableBal(Double availableBal) {
		this.availableBal = availableBal;
	}
	public Double getNewAddition() {
		return newAddition;
	}
	public void setNewAddition(Double newAddition) {
		this.newAddition = newAddition;
	}

	public Date getStockDate() {
		return stockDate;
	}

	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}

	public Double getInvoiceCost() {
		return invoiceCost;
	}

	public void setInvoiceCost(Double invoiceCost) {
		this.invoiceCost = invoiceCost;
	}

}
