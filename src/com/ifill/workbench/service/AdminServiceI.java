/**
 * 
 */
package com.ifill.workbench.service;

import java.util.Date;

import com.ifill.common.metadata.dao.ExpenseType;
import com.ifill.common.metadata.dao.Item;
import com.ifill.reports.vo.ReportsResponseWrapper;
import com.ifill.workbench.vo.BalanceSummary;
import com.ifill.workbench.vo.Pumps;

/**
 * @author Sandy
 *
 */
public interface AdminServiceI {

	public String configPumpById(String pumpid);
	public String addPump(Pumps pump);
	public String addProduct(Item item);
	String updateProduct(Item item);
	String deleteProduct(String key);
	String updatePrice(Item item);
	String deletePriceChange(String key);
	String deletePump(String key);
	ReportsResponseWrapper getPreviousBalances(Date date);
	String updateBalances(BalanceSummary bals);
	String createExpenseType(ExpenseType type);
	String deleteExpenseType(String key);
}
