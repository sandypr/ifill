/**
 * 
 */
package com.ifill.workbench.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.ifill.common.exceptions.DataAccessLayerException;
import com.ifill.common.metadata.dao.ExpenseType;
import com.ifill.common.metadata.dao.Item;
import com.ifill.common.utils.CommonUtils;
import com.ifill.common.utils.IfillConstants;
import com.ifill.reports.vo.ReportsResponseWrapper;
import com.ifill.workbench.vo.BalanceSummary;
import com.ifill.workbench.vo.Pumps;

/**
 * @author Sandy
 *
 */
@Service
public class AdminDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String CREATE_PUMP = "insert into pumps(pump_id, product_id, name, status) values(?,?,?,?)";
	private static String DELETE_PUMP = "update pumps set status = ? where pump_id = ? and product_id = ?";
	private static String ADD_PRODUCT = "insert into products(product_id, product_type, name, invoice_price, unit_price) values(?,?,?,?,?)";
	private static String UPDATE_PRODUCT = "update products set product_type=?, name=?, unit_price=? where product_id = ?";
	private static String UPDATE_PRICE = "update products set invoice_price=?, unit_price=? where product_id = ?";
	private static String UPDATE_PRICE_LOG = "insert into price_change_log(product_type, product_id, date_modified, new_invoice_price, new_unit_price) values (?,?,?,?,?)";
	private static String DELETE_PRODUCT = "delete from products where product_type = ? and product_id = ?";
	private static String DELETE_PRICE_CHANGE = "delete from price_change_log where product_id = ? and date_modified = ?";
	private static String GET_BALANCE_INITIALS = "select available_balance from balances where date = Date(?) - INTERVAL 1 DAY";
	private static String GET_CURRENTDAY_SALES = "select sales from balances where date = DATE(?)";
	private static String GET_BALANCE_SALES = "select sales from balances where date = DATE(?)";
	private static String GET_PREVIOUS_AVAI_BAL= "select available_balance from balances where date = DATE(?) - INTERVAL 1 DAY";
	private static String UPDATE_BALANCES = "update balances set rs_deposited = ?, rs_transfered = ?, new_additions = ?, available_balance = ? where date = DATE(?)";
	private static String CREATE_EXP_TYPE = "insert into expense_types values (?,?)";
	
	public void createPump(Pumps pump){
		System.out.println("creating pump");
		Object[] params = {pump.getPumpid(), pump.getFuelType(), pump.getPumpName(), pump.getStatus()};
		int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		try{
			jdbcTemplate.update(CREATE_PUMP, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public void deletePump(String key){
		
		// split key
		String[] compoundKeys = key.split(IfillConstants.CONSTANTS_KEY_SEPERATOR);
		
		Object[] params = {IfillConstants.CONSTANTS_INACTIVE, compoundKeys[1], compoundKeys[0] };
		int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		try{
			jdbcTemplate.update(DELETE_PUMP, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public void addProduct(Item item){
		
		Object[] params = {item.getProductId(), item.getProductType(), item.getName(), item.getInvoicePrice(), item.getUnitPrice()};
		int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.DOUBLE, Types.DOUBLE};
		try{
			jdbcTemplate.update(ADD_PRODUCT, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public void updateProduct(Item item){
		
		Object[] params = {item.getProductType(), item.getName(), item.getProductId()};
		int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		try{
			jdbcTemplate.update(UPDATE_PRODUCT, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public void deleteProduct(String key){
		
		// split key
		String[] compoundKeys = key.split(IfillConstants.CONSTANTS_KEY_SEPERATOR);
				
		Object[] params = {compoundKeys[0],compoundKeys[1]};
		int[] types = {Types.VARCHAR, Types.VARCHAR};

		try{
			jdbcTemplate.update(DELETE_PRODUCT, params, types);
		}catch(DataAccessException dae){
			//TODO
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	@Transactional
	public void updatePrice(Item item){
		
		Object[] params = {item.getInvoicePrice(), item.getUnitPrice(),item.getProductId() };
		int[] types = {Types.DOUBLE, Types.DOUBLE ,Types.VARCHAR};
		
		try{
			jdbcTemplate.update(UPDATE_PRICE, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		try{
			jdbcTemplate.update(UPDATE_PRICE_LOG, 
							new Object[]{item.getProductType(), item.getProductId(), 
											CommonUtils.getCurrentDate(), item.getInvoicePrice(), item.getUnitPrice()}, 
							new int[]{Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.DOUBLE, Types.DOUBLE });
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public void deletePriceChange(String key){
		
		// split key
		String[] compoundKeys = key.split(IfillConstants.CONSTANTS_KEY_SEPERATOR);
				
		Object[] params = {compoundKeys[0],compoundKeys[1]};
		int[] types = {Types.VARCHAR, Types.DATE};

		try{
			jdbcTemplate.update(DELETE_PRICE_CHANGE, params, types);
		}catch(DataAccessException dae){
			//TODO
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public Map<String, Double> getPreviousBalances(String date){
		
		String CONSTANTS_LASTBALANCE = "last_balance";
		Date previousDate = CommonUtils.getDate(date);		
		Object[] params = {previousDate};
		int[] types = {Types.DATE};
		
		List<Double> sales = new ArrayList<>();
		List<Double> previousAvailable = new ArrayList<>();
		Map<String,Double> currentDetails = new HashMap<>();
		try{
			previousAvailable = jdbcTemplate.queryForList(GET_BALANCE_INITIALS, Double.class, params, types);			
			sales = jdbcTemplate.queryForList(GET_CURRENTDAY_SALES, Double.class, params, types);
			currentDetails.put(IfillConstants.CONSTANTS_SALE, sales.get(0));
			currentDetails.put(CONSTANTS_LASTBALANCE, previousAvailable.get(0));
		}catch(DataAccessException dae){
			//TODO
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		return currentDetails;
	}
	
	// checks if the sales record is initiated for the date/day
	public ReportsResponseWrapper isBalanceRecordAvailable(Date curDate){
		
		Object[] params = {curDate};
		int[] types = {Types.DATE};
		
		Map<String, Object> map = new HashMap<>();
		List<Double> result = new ArrayList<>();
		Double sale = 0.0;
		Double previous = 0.0;
		try{
			//map = jdbcTemplate.queryForMap(GET_BALANCE_SALES, params, types);
			result = jdbcTemplate.query(GET_BALANCE_SALES,params,types,  new RowMapper<Double>() {
			      public Double mapRow(ResultSet resultSet, int i) throws SQLException {
			          return resultSet.getDouble(1);
			        }
			      });
			sale = (Double) result.get(0);
			
			result.clear();
			//map.clear();
			//map = jdbcTemplate.queryForMap(GET_PREVIOUS_AVAI_BAL, params, types);
			result = jdbcTemplate.query(GET_PREVIOUS_AVAI_BAL,params,types,  new RowMapper<Double>() {
			      public Double mapRow(ResultSet resultSet, int i) throws SQLException {
			          return resultSet.getDouble(1);
			        }
			      });
			previous = (Double) result.get(0);	
		}catch(DataAccessException dae){
			//TODO
			throw new DataAccessLayerException(dae.getMessage());
		}

		ReportsResponseWrapper resp = new ReportsResponseWrapper();
		resp.setStatus((sale != null && previous != null)? true : false);
		
		Map<String, String> respMap = new HashMap<>();
		if(sale != null){
			respMap.put("sale", sale.toString());
		}
		if(previous != null){
			respMap.put("previous", previous.toString());
		}
		
		resp.setReports(respMap);
		
		return resp;
	}
	
	public void updateBalances(BalanceSummary bals){
		
		Object[] params = {bals.getRsDeposited(),bals.getRsTransferred(),bals.getNewAddition(), bals.getAvailableBal(), bals.getDate()};
		int[] types = {Types.DOUBLE, Types.DOUBLE, Types.DOUBLE, Types.DOUBLE, Types.DATE};
		try{
			jdbcTemplate.update(UPDATE_BALANCES, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public void createExpType(ExpenseType type){
		
		Object[] params = {type.getExpType(), type.getExpName()};
		int[] types = {Types.VARCHAR, Types.VARCHAR};
		try{
			jdbcTemplate.update(CREATE_EXP_TYPE, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
	public void deleteExpType(String key){
		
		// split key
		String[] compoundKeys = key.split(IfillConstants.CONSTANTS_KEY_SEPERATOR);
		
		Object[] params = {IfillConstants.CONSTANTS_INACTIVE, compoundKeys[1], compoundKeys[0] };
		int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		try{
			jdbcTemplate.update(DELETE_PUMP, params, types);
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
	}
	
}
