/**
 * 
 */
package com.ifill.reports.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifill.common.exceptions.DataAccessLayerException;
import com.ifill.common.exceptions.ServiceLayerException;
import com.ifill.reports.dao.ReportsDao;
import com.ifill.reports.service.ReportsServiceI;
import com.ifill.reports.vo.ReportRespObject;
import com.ifill.reports.vo.ReportsResponseWrapper;
import com.ifill.workbench.vo.BalanceSummary;

/**
 * @author Sandy
 *
 */
@Service
public class ReportsServiceImpl implements ReportsServiceI{
	
	@Autowired
	private ReportsDao reportsDao;
	
	@Override
	public ReportsResponseWrapper generateCashPosition(Date reportDate){
		
		ReportsResponseWrapper respWrapper = null;
		
		try{
			Map<String,String> sales = reportsDao.generateCashReportData(reportDate);
			if(sales != null){
				respWrapper = new ReportsResponseWrapper();
				respWrapper.setReports(sales);
			}			
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return respWrapper;
	}

	@Override
	public List<ReportRespObject> generateSalesReport() {
		List<ReportRespObject> reportRespList;
		try{
			reportRespList = reportsDao.generateSalesReportData();
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportRespList;
	}
	
	@Override
	public List<ReportRespObject> generateSalesReport(Date date) {
		List<ReportRespObject> reportRespList;
		try{
			reportRespList = reportsDao.generateSalesReportData(date);
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportRespList;
	}

	@Override
	public ReportsResponseWrapper generateCreditReport(String custId) {
		
		ReportsResponseWrapper reportRespList;
		try{
			reportRespList = reportsDao.generateCreditData(custId);
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportRespList;
	}
	
	@Override
	public ReportsResponseWrapper generateDetailedCreditReport(Date reportDate) {
		ReportsResponseWrapper reportResp;
		try{
			reportResp = reportsDao.getDetailedCreditData(reportDate);
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportResp;
	}
	
	@Override
	public ReportsResponseWrapper generatePeriodicVariationData(Date fromDate, Date toDate) {
		ReportsResponseWrapper reportResp;
		try{
			reportResp = reportsDao.getPeriodicVerificationData(fromDate, toDate);
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportResp;
	}
	
	@Override
	public List<BalanceSummary> generateBalances(int month, int year) {
		List<BalanceSummary> reportResp;
		try{
			reportResp = reportsDao.getBalances(month, year);
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportResp;
	}
	
	@Override
	public List<BalanceSummary> generateInvoiceBalances(int month, int year) {
		List<BalanceSummary> reportResp;
		try{
			reportResp = reportsDao.getInvoiceBalances(month, year);
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportResp;
	}
	
	@Override
	public ReportsResponseWrapper generateDSRData(String fuel, int month, int year) {
		ReportsResponseWrapper reportResp;
		try{
			reportResp = reportsDao.getDSRData(fuel, month, year);
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportResp;
	}
	
	
	@Override
	public ReportsResponseWrapper getExpWiseData(String type, Date from, Date to) {
		ReportsResponseWrapper reportResp;
		try{
			reportResp = reportsDao.getExpWiseData(type, from, to);
						
		}catch(DataAccessLayerException dale){
			throw new ServiceLayerException(dale.getMessage());
		}
		
		return reportResp;
	}
}
