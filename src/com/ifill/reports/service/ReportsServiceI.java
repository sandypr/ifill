/**
 * 
 */
package com.ifill.reports.service;

import java.util.Date;
import java.util.List;

import com.ifill.reports.vo.ReportRespObject;
import com.ifill.reports.vo.ReportsResponseWrapper;
import com.ifill.workbench.vo.BalanceSummary;

/**
 * @author Sandy
 *
 */
public interface ReportsServiceI {

	ReportsResponseWrapper generateCashPosition(Date reportDate);
	List<ReportRespObject> generateSalesReport();
	List<ReportRespObject> generateSalesReport(Date date);
	ReportsResponseWrapper generateCreditReport(String custId);
	ReportsResponseWrapper generateDetailedCreditReport(Date reportDate);
	ReportsResponseWrapper generatePeriodicVariationData(Date fromDate,	Date endDate);
	List<BalanceSummary> generateBalances(int month, int year);
	List<BalanceSummary> generateInvoiceBalances(int month, int year);
	ReportsResponseWrapper generateDSRData(String fuel, int month, int year);
	ReportsResponseWrapper getExpWiseData(String type, Date from, Date to);
}
