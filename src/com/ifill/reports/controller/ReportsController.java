/**
 * 
 */
package com.ifill.reports.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ifill.common.exceptions.ServiceLayerException;
import com.ifill.common.utils.CommonUtils;
import com.ifill.common.utils.VirtualProxy;
import com.ifill.reports.service.ReportsServiceI;
import com.ifill.reports.vo.ReportReqObject;
import com.ifill.reports.vo.ReportRespObject;
import com.ifill.reports.vo.ReportsResponseWrapper;
import com.ifill.userMgmt.service.ProfileMgmtI;
import com.ifill.workbench.vo.BalanceSummary;

/**
 * @author Sandy
 *
 */
@Controller
@RequestMapping("/reports")
public class ReportsController {
	@Autowired
	ReportsServiceI reportsService;
	
	@Autowired
	private ProfileMgmtI profileMgmtService;
	
	private static String VIEWS_PERIODIC_REPORT = "showPeriodicReport";
	private static String VIEWS_BALANCE_REPORT = "showBalanceReport";
	private static String VIEWS_INVOICE_BALANCE = "invoiceBalanceReport";
	private static String VIEWS_EXPWISE_RPT = "showExpReport";
	private static String VIEWS_EXPWISE_REPORT = "admin_ui/ExpenseWiseReport";
	private static String VIEWS_APP_HOME = "index";
	private static String VIEWS_CASH_REPORT_STMT = "CashPositionStatement";
	private static String VIEWS_CASH_REPORT = "showCashPositionStmt";
	private static boolean needSessionAttrs = false;
	
	@RequestMapping(method=RequestMethod.GET, value="/showCashPositionStmt",  produces="application/json")
	public String showDailyCashPosition(HttpServletRequest req, HttpServletResponse resp){
		if(!needSessionAttrs)
			clearSession(req);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		System.out.println(req.getSession().getAttribute("ReportDate"));
		genCashPositionData(req,resp);	
		needSessionAttrs = false;
		return "CashPositionStatement";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/salesReportData",  produces="application/json")
	public  @ResponseBody List<ReportRespObject> retrieveSalesData(HttpServletRequest req, HttpServletResponse resp){
		
		List<ReportRespObject> reportRespList;
		try{
			reportRespList = reportsService.generateSalesReport(CommonUtils.getCurrentDate());
		}catch(ServiceLayerException sle){
			return null;
		}
		return reportRespList;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/salesReportData/{reportDate}",  produces="application/json")
	public  @ResponseBody List<ReportRespObject> retrieveSalesData(@PathVariable("reportDate") String reportDate, HttpServletRequest req, HttpServletResponse resp){
		
		Date report_Date = CommonUtils.getDate(reportDate);
		List<ReportRespObject> reportRespList;
		try{
			reportRespList = reportsService.generateSalesReport(report_Date);
		}catch(ServiceLayerException sle){
			return null;
		}
		return reportRespList;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showSalesReport",  produces="application/json")
	public String showSalesReportView(HttpServletRequest req, HttpServletResponse resp){
		
		clearSession(req);		
		resp.addHeader("Access-Control-Allow-Origin", "*");
		
		// add date details to session
		String date = CommonUtils.getCurrentDateAsString();
		HttpSession session = req.getSession();
		if(session.getAttribute("ReportDate") == null){
			session.setAttribute("ReportDate", date);
		}
		if(session.getAttribute("Date") == null)
			session.setAttribute("Date", date);
		
		return "DailySalesReport";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="showMiscReport")
	public String showMiscReports(HttpServletRequest req){
		if(profileMgmtService.isUserLoggedIn(req.getSession())){
			clearSession(req);
			return "MiscReports";
		}
		return "index";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/showReport", consumes = "application/json")
	public @ResponseBody String pushStockReciept(@RequestBody ReportReqObject feed, HttpServletRequest req, HttpServletResponse resp) throws ParseException, IOException{

		clearSession(req);
		HttpSession session = req.getSession();

		// add data necessary to session
		if(feed.getDate() != null){
			String dt = CommonUtils.getFormattedDateAsString(feed.getDate());
			session.setAttribute("ReportDate", dt);
		}
		
		if(feed.getEndDate() != null){
			session.setAttribute("fromDate", CommonUtils.getFormattedDateAsString(feed.getDate()));
			session.setAttribute("toDate", CommonUtils.getFormattedDateAsString(feed.getEndDate()));		
		}
		
		if(feed.getMonth() != null){
			session.setAttribute("month_year", feed.getMonth());
		}
		
		needSessionAttrs = true;
		
		System.out.println("session report date :"+session.getAttribute("ReportDate"));
		System.out.println("session date :"+session.getAttribute("Date"));
		
		// redirect to views
		if(feed.getType().equals("periodic_report")){
			System.out.println("opening periodic report");
			return VIEWS_PERIODIC_REPORT;
		}
		
		if(feed.getType().equals("sales_report")){	
			System.out.println("opening sales report");
			return "showSalesReport";
		} 
		
		if(feed.getType().equals("balance_report")){
			System.out.println("opening balance report");
			return VIEWS_BALANCE_REPORT;
		}
		
		if(feed.getType().equals("invoice_balance_report")){
			System.out.println("opening invoice balance report");
			return VIEWS_INVOICE_BALANCE;
		}
		
		if(feed.getType().equals("expenseWise_report") && profileMgmtService.isUserAuthorized(session, true)){
			session.setAttribute("expType", feed.getSubType());
			return VIEWS_EXPWISE_RPT;
		}
		
		if(feed.getType().equals("cash_report")){
			return VIEWS_CASH_REPORT;
		}
		
		return null;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showBalanceReport")
	public String showBalanceReport(HttpServletRequest req){
		if(profileMgmtService.isUserLoggedIn(req.getSession())){
			clearSession(req);
			return "admin_ui/BalanceSheet";
		}
		return "index";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/invoiceBalanceReport")
	public String showInvoiceBalanceReport(HttpServletRequest req){
		if(profileMgmtService.isUserLoggedIn(req.getSession())){
			clearSession(req);
			return "InvoiceBalances";
		}
		return "index";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showCreditReport")
	public String showCreditReport(HttpServletRequest req){
		if(profileMgmtService.isUserLoggedIn(req.getSession())){
			clearSession(req);
			return "CreditReports";
		}
		return "index";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showCreditStatement",  produces="application/json")
	public String showCreditReportView(HttpServletRequest req, HttpServletResponse resp){
		resp.addHeader("Access-Control-Allow-Origin", "*");
		// add date details to session
		HttpSession session = req.getSession();
		String date = CommonUtils.getCurrentDateAsString();
		session.setAttribute("Date", date);
		
		return "CreditStatement";
	}
	
	@RequestMapping(method=RequestMethod.POST, value="postCreditDetails")
	public @ResponseBody void showCreditReport(@RequestBody ReportReqObject feed, HttpServletRequest req){
		
		clearSession(req);
		// add customer id details to session
		HttpSession session = req.getSession();		
		session.setAttribute("customerID", feed.getId());
		session.setAttribute("customerName", feed.getName());
		String date = CommonUtils.getCurrentDateAsString();
		session.setAttribute("Date", date);
		
		//return "CreditStatement";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/salesCreditData/{customer}",  produces="application/json")
	public  @ResponseBody ReportsResponseWrapper retrieveCreditData(@PathVariable("customer") String customer, HttpServletRequest req, HttpServletResponse resp){
		
		clearSession(req);
		ReportsResponseWrapper reportRespList;
		try{
			reportRespList = reportsService.generateCreditReport(customer);
		}catch(ServiceLayerException sle){
			return null;
		}
		return reportRespList;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/creditReportDetails",  produces="application/json")
	public  @ResponseBody ReportsResponseWrapper retrieveDetailedCreditData(HttpServletRequest req, HttpServletResponse resp){
		
		HttpSession session = req.getSession();	
		Date report_Date = CommonUtils.getDate(session.getAttribute("ReportDate").toString());
		ReportsResponseWrapper reportResp;
		try{
			reportResp = reportsService.generateDetailedCreditReport(report_Date);
		}catch(ServiceLayerException sle){
			return null;
		}
		return reportResp;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="showPeriodicReport")
	public String showPeriodicReport(HttpServletRequest req, HttpServletResponse resp){
		resp.addHeader("Access-Control-Allow-Origin", "*");
		needSessionAttrs = false;
		return "PeriodicVariationReport";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="showPeriodicVariationData")
	public @ResponseBody ReportsResponseWrapper showPeriodicVariationData(HttpServletRequest req){
		
		// get period details from session
		HttpSession session = req.getSession();		
		Date fromDate = CommonUtils.getDate(session.getAttribute("fromDate").toString());
		Date toDate = CommonUtils.getDate(session.getAttribute("toDate").toString());
		
		ReportsResponseWrapper reportResp;
		try{
			reportResp = reportsService.generatePeriodicVariationData(fromDate, toDate);
		}catch(ServiceLayerException sle){
			return null;
		}
		return reportResp;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showBalanceData",  produces="application/json")
	public @ResponseBody List<BalanceSummary> showBalanceData(HttpServletRequest req){
		List<BalanceSummary> reportResp = null;
		HttpSession sess = req.getSession();
		String[] tokens = (sess.getAttribute("month_year").toString()).split(" ");
		try{
			reportResp = reportsService.generateBalances(CommonUtils.convertMonthToDigit(tokens[0]), Integer.parseInt(tokens[1]));
		}catch(ServiceLayerException | NumberFormatException | ParseException sle){
			return null;
		}
		return reportResp;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showInvoiceBalanceData",  produces="application/json")
	public @ResponseBody List<BalanceSummary> showInvoiceBalanceData(HttpServletRequest req){
		List<BalanceSummary> reportResp = null;
		HttpSession sess = req.getSession();
		String[] tokens = (sess.getAttribute("month_year").toString()).split(" ");
		
		try{
			reportResp = reportsService.generateInvoiceBalances(CommonUtils.convertMonthToDigit(tokens[0]), Integer.parseInt(tokens[1]));
		}catch(ServiceLayerException | NumberFormatException | ParseException sle){
			return null;
		}
		return reportResp;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showDSRReport")
	public String showDSRReport(HttpServletRequest req){
		if(profileMgmtService.isUserLoggedIn(req.getSession())){
			clearSession(req);
			return "DSRMain";
		}
		return "index";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/postDSRQuery", consumes = "application/json")
	public @ResponseBody String postDSRQuery(@RequestBody ReportReqObject feed, HttpServletRequest req, HttpServletResponse resp) throws ParseException, IOException{
		// add date details to session
		HttpSession session = req.getSession();
		
		if(feed.getMonth() != null){
			session.setAttribute("month_year", feed.getMonth());
		}
		
		if(feed.getType() != null){
			session.setAttribute("dsrFuel", feed.getType());
		}
		
		return "showDSRView";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/dsrData",  produces="application/json")
	public @ResponseBody ReportsResponseWrapper generateDRSData(HttpServletRequest req){
		System.out.println("getting dsr Data");
		ReportsResponseWrapper reportResp = null;
		HttpSession sess = req.getSession();
		String[] tokens = (sess.getAttribute("month_year").toString()).split(" ");
		String fuel = sess.getAttribute("dsrFuel").toString();
		
		try{
			System.out.println(CommonUtils.convertMonthToDigit(tokens[0])+"-"+Integer.parseInt(tokens[1]));
			reportResp = reportsService.generateDSRData(fuel, CommonUtils.convertMonthToDigit(tokens[0]), Integer.parseInt(tokens[1]));
		}catch(ServiceLayerException | NumberFormatException | ParseException sle){
			return null;
		}
		return reportResp;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="showDSRView")
	public String showDSRView(HttpServletRequest req, HttpServletResponse resp){
		resp.addHeader("Access-Control-Allow-Origin", "*");
		clearSession(req);
		return "DSRReport";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/backTrack")
	public @ResponseBody int showBackTrackingDays(HttpServletRequest req){
		int days = 0;
		if(profileMgmtService.isUserLoggedIn(req.getSession())){
			VirtualProxy vProxy = VirtualProxy.getInstance();
			days = Integer.parseInt(vProxy.getValue("date.backward.days"));
		}
		return days;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/showExpReport")
	public String showdExpenseWiseReport(HttpServletRequest req){
		if(profileMgmtService.isUserAuthorized(req.getSession(),true)){
			needSessionAttrs = false;
			return VIEWS_EXPWISE_REPORT;
		}
		else
			return VIEWS_APP_HOME;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/expWiseData",  produces="application/json")
	public @ResponseBody ReportsResponseWrapper generateExpWiseData(HttpServletRequest req){
		ReportsResponseWrapper reportResp = null;
		HttpSession sess = req.getSession();
		Date from = CommonUtils.getDate(sess.getAttribute("fromDate").toString());
		Date to = CommonUtils.getDate(sess.getAttribute("toDate").toString());
		
		String type = sess.getAttribute("expType").toString();
		try{
			if(type.equalsIgnoreCase("All"))
				reportResp = reportsService.getExpWiseData(null, from, to);
			else
				reportResp = reportsService.getExpWiseData(type, from, to);
		}catch(ServiceLayerException | NumberFormatException sle){
			return null;
		}
		return reportResp;
	}
	
	private void genCashPositionData(HttpServletRequest req, HttpServletResponse resp){
		
		HttpSession session = req.getSession();
		String date = CommonUtils.getCurrentDateAsString();
		Date reportDate = CommonUtils.getDate(date);
		if(session.getAttribute("ReportDate") != null){
			reportDate = CommonUtils.getDate(session.getAttribute("ReportDate").toString());
		}else{ // if null
			session.setAttribute("ReportDate",reportDate);
		}
		if(session.getAttribute("Date") == null){
			session.setAttribute("Date", date);
		}
		System.out.println("report date :"+reportDate);
		System.out.println("date :"+date);
		ReportsResponseWrapper respWrapper = null;
		try{
			respWrapper = reportsService.generateCashPosition(reportDate);
		}catch(ServiceLayerException sle){
			throw sle;
		}

		session.setAttribute("resp", respWrapper.getReports());
		session.setAttribute("MS", respWrapper.getValue("MS"));
		session.setAttribute("HSD", respWrapper.getValue("HSD"));
		session.setAttribute("CREDITS", respWrapper.getValue("CREDITS"));
		session.setAttribute("LUBES", respWrapper.getValue("LUBES"));
		session.setAttribute("CREDIT_RECOVERY", respWrapper.getValue("CREDIT_RECOVERY"));
		session.setAttribute("MISC_EXPENSES", respWrapper.getValue("MISC_EXPENSES"));
		
	}
	
	private void clearSession(HttpServletRequest req){
		HttpSession session = req.getSession();
		session.removeAttribute("ReportDate");
		session.removeAttribute("Date");
		session.removeAttribute("fromDate");
		session.removeAttribute("toDate");
		session.removeAttribute("month_year");
	}

}
