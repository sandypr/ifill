/**
 * 
 */
package com.ifill.reports.dao;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.ifill.common.exceptions.DataAccessLayerException;
import com.ifill.common.metadata.dao.MetadataDao;
import com.ifill.common.utils.CommonUtils;
import com.ifill.common.utils.IfillConstants;
import com.ifill.reports.vo.ReportRespObject;
import com.ifill.reports.vo.ReportsResponseWrapper;
import com.ifill.workbench.vo.BalanceSummary;

/**
 * @author Sandy
 *
 */
@Service
public class ReportsDao {
	
	private static String SUM_DAILY_SALES_FROM_PUMP_READINGS = "SELECT coalesce(SUM(effc_sale_in_rs),0) sale from daily_pump_readings where day_date = DATE(?) AND pump_id in (SELECT pump_id from pumps where status = 'ACTIVE' AND product_id = ?)";
	private static String SUM_DAILY_CREDITS = "SELECT coalesce(SUM(total_amount_of_sale),0) sale FROM sales where sale_type = 'CREDIT' AND date_of_sale = DATE(?)";
	private static String SUM_DAILY_CREDIT_RECOVERYS = "SELECT coalesce(SUM(amount),0) sale FROM credit_recovery where date_of_recovery = DATE(?)";
	private static String SUM_DAILY_LUBES_SALE = "SELECT coalesce(SUM(sales_in_rs),0) sale FROM lubes_sales where date = DATE(?)";
	private static String SUM_DAILY_EXPENSES = "select coalesce(SUM(amount),0) sale FROM misc_expenses where expns_date = DATE(?);";
	private static String CREDITSALE_DETAILS = "SELECT s.product, SUM(s.quantity_of_sale) as quantity, SUM(s.total_amount_of_sale) as sale from sales s WHERE s.sale_type = 'CREDIT' and s.date_of_sale = DATE(?) GROUP BY s.product ORDER BY s.product DESC";
	private static String DIP_DETAILS = "select fuel,date,dip from dailydips where date in (Date(?),Date(?))";
	private static String STOCK_RECEIPT = "SELECT product_id fuel,SUM(quantity) stock FROM stock_reciept where reciept_date BETWEEN DATE(?) AND DATE(?) GROUP BY product_id";
	private static String METER_SALES = "SELECT p.pump_id, p.product_id, r.reading_in_liters from (SELECT pump_id, product_id from pumps) p, (SELECT pump_id, reading_in_liters from daily_pump_readings where day_date IN (DATE(?),DATE(?)) order BY pump_id, reading_in_liters) r where p.pump_id = r.pump_id ORDER BY p.product_id, p.pump_id, r.reading_in_liters";
	
	private static String READINGS_FOR_SALES_REPORT_PUMPS = "SELECT readings.*, pmps.name, pmps.product_id, 'Fuel' type from (SELECT CUR.pump_id, yes.opening, CUR.closing, (cur.closing - yes.opening) sales, cur.pump_test,totalSale, saleRs  from (SELECT pump_id, reading_in_liters closing, pump_test, effc_sale_in_liters totalSale, effc_sale_in_rs saleRs from daily_pump_readings where day_date = DATE(?)) cur, (SELECT pump_id,reading_in_liters opening from daily_pump_readings where day_date = DATE(?) - INTERVAL 1 DAY) yes WHERE cur.pump_id = yes.pump_id) readings, pumps pmps WHERE pmps.pump_id = readings.pump_id";
	private static String READINGS_FOR_SALES_REPORT_LUBES = "SELECT product, units, sales_in_rs, 'Lube' type from lubes_sales where date = DATE(?)";
	
	private static String CREDIT_REPORT = "SELECT tbl.* FROM ((SELECT customer_id, date_of_sale AS date,total_amount_of_sale as credit, 0 as recovery  from sales where sale_type = ? and customer_id = ?)  UNION "
											+"(SELECT customer_id, date_of_recovery as date, 0 as credit, amount as recovery from credit_recovery where customer_id = ?) ) tbl ORDER BY tbl.date";
	
	private static String GET_BALANCE_PER_MONTH = "SELECT bal.date, pre.available_balance previous, bal.sales sale, bal.rs_deposited depositedInBank, bal.rs_transfered transferredToCompany, bal.new_additions newAmountToManager, bal.available_balance balanceLeft "
													+"  from balances bal INNER JOIN balances pre ON pre.date = DATE(bal.date) - INTERVAL 1 DAY"
													+"  where MONTH(bal.date) = ? and YEAR(bal.date) = ? ORDER BY bal.date ASC";
	
	private static String GET_INVOICE_BALANCES = "select date, transfered_to_company, stock_date, invoice_cost, balance_with_company from invoice_balances where year(date) = ? and month(date) = ? order by date";
	
	private static String GET_DSR_DATA = "SELECT c.day_date, d.dip, c.stock, (d.dip+c.stock) total_stock, (c.meter_sale-c.pumptest) meter_sales, c.pumptest, c.meter_sale net_sale from "
											+" (SELECT a.day_date, b.stock, a.meter_sale, a.pumptest "
											+ " from "
											+ " (SELECT day_date, SUM(effc_sale_in_liters) meter_sale, SUM(pump_test) pumptest from daily_pump_readings where pump_id IN (select pump_id from pumps where product_id = ?) AND MONTH(day_date) = ? AND YEAR(day_date) = ?) a,"
											+ " (SELECT SUM(quantity) stock , reciept_date from stock_reciept where product_id = ? and MONTH(reciept_date) = ? AND YEAR(reciept_date) = ?) b "
											+ " where a.day_date = b.reciept_date) c, "
											+ " (SELECT Date,Dip from dailydips where Fuel = ? and month(date) = ? and year(date) = ?) d "
										+ " where c.day_date = d.Date"
										+ " order by c.day_date;";
	
	private static String GET_EXPWISE_REPORT_DATA = "SELECT expense_type, description, amount, expns_date from misc_expenses where expense_type = ? and DATE(expns_date) >= DATE(?) and DATE(expns_date) <= DATE(?)";
	private static String GET_EXP_REPORT_DATA = "SELECT expns_date, expense_id, expense_type, description, amount from misc_expenses where DATE(expns_date) >= DATE(?) and DATE(expns_date) <= DATE(?)";
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	MetadataDao metadataDao;
	
	// daily cash position
	public Map<String,String> generateCashReportData(Date reportDate){

		Map<String,String> sales = new HashMap<>();

		// pull sales from pump readings
		try{
			List<Map<String,Object>> result = jdbcTemplate.queryForList(SUM_DAILY_SALES_FROM_PUMP_READINGS,new Object[]{reportDate, "MS"});
			
			for(Map<String,Object> row : result){
				sales.put("MS", row.get("sale").toString());
			}
			
			result = null;
			result = jdbcTemplate.queryForList(SUM_DAILY_SALES_FROM_PUMP_READINGS,new Object[]{reportDate, "HSD"});
			
			for(Map<String,Object> row : result){
				sales.put("HSD", row.get("sale").toString());
			}
			
			
			// pull lubes sale
			result = null;
			result = jdbcTemplate.queryForList(SUM_DAILY_LUBES_SALE,new Object[]{reportDate});
			
			for(Map<String,Object> row : result){
				sales.put("LUBES", row.get("sale").toString());
			}
			
			// pull credit sales
			result = null;
			result = jdbcTemplate.queryForList(SUM_DAILY_CREDITS,new Object[]{reportDate});
			
			for(Map<String,Object> row : result){
				sales.put("CREDITS", row.get("sale").toString());
			}
			
			/*result = null;
			result = jdbcTemplate.queryForList(SUM_DAILY_CREDITS);
			
			for(Map<String,Object> row : result){
				sales.put("MS_CREDIT", row.get("sale").toString());
			}*/
			
			// pull credit recoveries
			result = null;
			result = jdbcTemplate.queryForList(SUM_DAILY_CREDIT_RECOVERYS,new Object[]{reportDate});
			
			for(Map<String,Object> row : result){
				sales.put("CREDIT_RECOVERY", row.get("sale").toString());
			}
			
			//pull misc expenses
			result = null;
			result = jdbcTemplate.queryForList(SUM_DAILY_EXPENSES,new Object[]{reportDate});
			
			for(Map<String,Object> row : result){
				sales.put("MISC_EXPENSES", row.get("sale").toString());
			}
			
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		return sales;
	}
	
	// sales report data
	public List<ReportRespObject> generateSalesReportData(){

		List<ReportRespObject> reportRespList = new ArrayList<>();

		// pull sales from pump readings
		try{
			List<Map<String,Object>> result = jdbcTemplate.queryForList(READINGS_FOR_SALES_REPORT_PUMPS);
			
			for(Map<String,Object> row : result){
				
				String pump = row.get("pump_id").toString();
				String type = row.get("type").toString();
				String product = row.get("product_id").toString();
				double opening = Double.valueOf(row.get("opening").toString());
				double closing = Double.valueOf(row.get("closing").toString());			
				double sales = Double.valueOf(row.get("sales").toString());
				double test = Double.valueOf(row.get("pump_test").toString());
				double totalSale = Double.valueOf(row.get("totalSale").toString());
				double saleRs = Double.valueOf(row.get("saleRs").toString());
				
				ReportRespObject reportRow = new ReportRespObject();
				
				reportRow.setPumpId(pump);
				reportRow.setType(type);
				reportRow.setProduct(product);
				reportRow.setOpening(opening);
				reportRow.setClosing(closing);
				reportRow.setSaleLtr(sales);
				reportRow.setTest(test);
				reportRow.setFinalQuantity(totalSale);
				reportRow.setSaleRs(saleRs);
				
				reportRespList.add(reportRow);
			}
			
			result = null;
			result = jdbcTemplate.queryForList(READINGS_FOR_SALES_REPORT_LUBES);
			
			for(Map<String,Object> row : result){
				
				String product = row.get("product").toString();
				double finalQ = Double.valueOf(row.get("units").toString());
				String type = row.get("type").toString();
				double saleRs = Double.valueOf(row.get("sales_in_rs").toString());
				
				ReportRespObject reportRow = new ReportRespObject();
				reportRow.setType(type);
				reportRow.setProduct(product);
				reportRow.setFinalQuantity(finalQ);
				reportRow.setSaleRs(saleRs);
				
				reportRespList.add(reportRow);
			}
			
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		return reportRespList;
	}
	
	public List<ReportRespObject> generateSalesReportData(Date date){

		List<ReportRespObject> reportRespList = new ArrayList<>();

		// pull sales from pump readings
		try{
			
			List<Map<String,Object>> result = jdbcTemplate.queryForList(READINGS_FOR_SALES_REPORT_PUMPS,new Object[]{date,date},new int[]{Types.DATE,Types.DATE});
			
			for(Map<String,Object> row : result){
				
				String pump = row.get("pump_id").toString();
				String type = row.get("type").toString();
				String product = row.get("product_id").toString();
				String pumpName = row.get("name").toString();
				double opening = Double.valueOf(row.get("opening").toString());
				double closing = Double.valueOf(row.get("closing").toString());			
				double sales = Double.valueOf(row.get("sales").toString());
				double test = Double.valueOf(row.get("pump_test").toString());
				double totalSale = Double.valueOf(row.get("totalSale").toString());
				double saleRs = Double.valueOf(row.get("saleRs").toString());
				
				ReportRespObject reportRow = new ReportRespObject();
				
				reportRow.setPumpId(pump);
				reportRow.setPumpName(pumpName);
				reportRow.setType(type);
				reportRow.setProduct(product);
				reportRow.setOpening(opening);
				reportRow.setClosing(closing);
				reportRow.setSaleLtr(sales);
				reportRow.setTest(test);
				reportRow.setFinalQuantity(totalSale);
				reportRow.setSaleRs(saleRs);
				
				reportRespList.add(reportRow);
			}
			
			result = null;			
			result = jdbcTemplate.queryForList(READINGS_FOR_SALES_REPORT_LUBES,new Object[]{date},new int[]{Types.DATE});
			
			for(Map<String,Object> row : result){
				
				String product = row.get("product").toString();
				double finalQ = Double.valueOf(row.get("units").toString());
				String type = row.get("type").toString();
				double saleRs = Double.valueOf(row.get("sales_in_rs").toString());
				
				ReportRespObject reportRow = new ReportRespObject();
				reportRow.setType(type);
				reportRow.setProduct(product);
				reportRow.setFinalQuantity(finalQ);
				reportRow.setSaleRs(saleRs);
				
				reportRespList.add(reportRow);
			}
			
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		return reportRespList;
	}
	
	public ReportsResponseWrapper generateCreditData(String customerID){
		
		List<Map<String, Object>> rows = null;
		try{
			rows = jdbcTemplate.queryForList(CREDIT_REPORT, new Object[]{"CREDIT",customerID,customerID}, new int[] {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR});
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		// process rows
		ReportsResponseWrapper respWrapper = new ReportsResponseWrapper();
		List<ReportRespObject> listOfObjs = new ArrayList<>();
		
		for(Map<String,Object> row : rows){
			ReportRespObject respObj = new ReportRespObject();
			String custId = row.get("customer_id").toString();
			
			respObj.setCustomerID(custId);
			respObj.setDate((Date)row.get("date"));
			respObj.setCredit(Double.parseDouble(row.get("credit").toString()));
			respObj.setRecovery(Double.parseDouble(row.get("recovery").toString()));
			
			listOfObjs.add(respObj);			
		}
		
		respWrapper.setReportRespList(listOfObjs);
		
		return respWrapper;
		
	}
	
	// detailed credit sales
	public ReportsResponseWrapper getDetailedCreditData(Date reportDate){
		ReportsResponseWrapper wrapper = new ReportsResponseWrapper();
		List<ReportRespObject> reportRespList = null;
		
		List<Map<String, Object>> rows = null;
		try{
			rows = jdbcTemplate.queryForList(CREDITSALE_DETAILS, new Object[]{reportDate}, new int[]{Types.DATE});
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		for(Map<String,Object> row : rows){
			ReportRespObject respObj = new ReportRespObject();
			
			respObj.setProduct(row.get(IfillConstants.CONSTANTS_PRODUCT).toString());
			respObj.setSaleLtr(Double.parseDouble(row.get(IfillConstants.CONSTANTS_QUANTITY).toString()));
			respObj.setSaleRs(Double.parseDouble(row.get(IfillConstants.CONSTANTS_SALE).toString()));
			
			if(reportRespList == null) reportRespList = new ArrayList<>();
			
			reportRespList.add(respObj);			
		}
		
		wrapper.setReportRespList(reportRespList);
		
		return wrapper;
	}
	
	public ReportsResponseWrapper getPeriodicVerificationData(Date fromDate, Date toDate){
		
		ReportsResponseWrapper respWrapper = new ReportsResponseWrapper();
		Map<String,String> data = new HashMap<>();
		List<Map<String, Object>> rows = null;
		try{
			
			// dip details
			rows = jdbcTemplate.queryForList(DIP_DETAILS, new Object[]{fromDate,toDate}, new int[]{Types.DATE,Types.DATE});
			
			// TODO  add fuel keys
			data.put("keys", "MS,HSD");
			
			for(Map<String,Object> row : rows){
				String date = row.get(IfillConstants.CONSTANTS_DATE).toString();
				Date checkDate = CommonUtils.getCustomDate("yyyy-MM-dd", date);
				
				String fuel = row.get(IfillConstants.CONSTANTS_FUEL).toString();
				String prefix = IfillConstants.CONSTANTS_HSD;
				if(fuel.equals(IfillConstants.CONSTANTS_MS))
					prefix = IfillConstants.CONSTANTS_MS;
				
				if(checkDate.equals(fromDate)){
					data.put(prefix+"_FROM_DATE", date);
					data.put(prefix+"_FROM_DIP", row.get(IfillConstants.CONSTANTS_DIPS).toString());					
				}
				
				if(checkDate.equals(toDate)){
					data.put(prefix+"_TO_DATE", date);
					data.put(prefix+"_TO_DIP", row.get(IfillConstants.CONSTANTS_DIPS).toString());
				}
			}
			
			// add stock reciept
			rows = jdbcTemplate.queryForList(STOCK_RECEIPT, new Object[]{fromDate,toDate}, new int[]{Types.DATE,Types.DATE});
			for(Map<String,Object> row : rows){
				String fuel = row.get(IfillConstants.CONSTANTS_FUEL).toString();
				data.put(fuel+"_TOTAL_STOCK", row.get(IfillConstants.CONSTANTS_STOCK).toString());
			}
			
			respWrapper.setReports(data);
			
			// meter sales
			rows = jdbcTemplate.queryForList(METER_SALES, new Object[]{fromDate,toDate}, new int[]{Types.DATE,Types.DATE});
			
			List<ReportRespObject> reportRespList = new ArrayList<>();
			ReportRespObject respObj = null;
			for(Map<String,Object> row : rows){
				if(respObj == null){
					respObj = new ReportRespObject();
					respObj.setOpening(Double.parseDouble(row.get(IfillConstants.CONSTANTS_READING_IN_METERS).toString()));
					respObj.setProduct(row.get(IfillConstants.CONSTANTS_PRODUCT_ID).toString());
					respObj.setPumpId(row.get(IfillConstants.CONSTANTS_PUMP_ID).toString());
				}else {
					// TODO if there is a single record, do some thing					
					
					respObj.setClosing(Double.parseDouble(row.get(IfillConstants.CONSTANTS_READING_IN_METERS).toString()));
					reportRespList.add(respObj);
					
					respObj = null;
				}
			}
			
			respWrapper.setReportRespList(reportRespList);
			
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		return respWrapper;
	}
	
	public List<BalanceSummary> getBalances(int month, int year){
		
		String c_depositedInBank = "depositedInBank";
		String c_transferedToCompany = "transferredToCompany";
		String c_newAmount = "newAmountToManager";
		String c_balance = "balanceLeft";
		String c_previous = "previous";
		
		Object[] params = {month, year};
		int[] types = {Types.INTEGER, Types.INTEGER};
		
		List<Map<String, Object>> rows = null;
		List<BalanceSummary> balances = new ArrayList<>();
		try{
			rows = jdbcTemplate.queryForList(GET_BALANCE_PER_MONTH, params, types);
			
			for(Map<String,Object> row : rows){
				BalanceSummary bal = new BalanceSummary();
				bal.setDate((Date)row.get(IfillConstants.CONSTANTS_DATE));
				bal.setSale((Double)row.get(IfillConstants.CONSTANTS_SALE));
				bal.setRsDeposited((Double)row.get(c_depositedInBank));
				bal.setRsTransferred((Double)row.get(c_transferedToCompany));
				bal.setNewAddition((Double)row.get(c_newAmount));
				bal.setAvailableBal((Double)row.get(c_balance));
				bal.setBalanceYesterday((Double)row.get(c_previous));
				
				balances.add(bal);
			}
			
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		return balances;
	}
	
public List<BalanceSummary> getInvoiceBalances(int month, int year){
		
		String c_stockDate = "stock_date";
		String c_transferedToCompany = "transferredToCompany";
		String c_invoiceCost = "invoice_cost";
		String c_balance = "balance_with_company";
		
		Object[] params = {month, year};
		int[] types = {Types.INTEGER, Types.INTEGER};
		
		List<Map<String, Object>> rows = null;
		List<BalanceSummary> balances = new ArrayList<>();
		try{
			rows = jdbcTemplate.queryForList(GET_INVOICE_BALANCES, params, types);
			
			for(Map<String,Object> row : rows){
				BalanceSummary bal = new BalanceSummary();
				
				bal.setDate((Date)row.get(IfillConstants.CONSTANTS_DATE));
				bal.setStockDate((Date)row.get(c_stockDate));
				bal.setInvoiceCost((Double)row.get(c_invoiceCost));
				bal.setRsTransferred((Double)row.get(c_transferedToCompany));
				bal.setAvailableBal((Double)row.get(c_balance));
				
				balances.add(bal);
			}
			
		}catch(DataAccessException dae){
			dae.printStackTrace();
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		return balances;
	}
	
	public ReportsResponseWrapper getDSRData(String fuel, int month, int year){
		
		ReportsResponseWrapper wrapper = new ReportsResponseWrapper();
		List<ReportRespObject> reportRespList = new ArrayList<>();
		Object[] params = {fuel,month, year, fuel,month, year, fuel,month, year};
		int[] types = {Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER};
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(GET_DSR_DATA, params, types);
			for(Map<String, Object> row : rows){
				ReportRespObject respRow = new ReportRespObject();
				respRow.setDate(CommonUtils.getDate(row.get("day_date").toString()));
				respRow.setDip((Double)row.get("dip"));
				respRow.setReciept((Double)row.get("stock"));
				respRow.setTotalStock((Double) row.get("total_stock"));
				respRow.setClosing((Double)row.get("meter_sales"));
				respRow.setTest((Double)row.get("pumptest"));
				respRow.setSaleLtr((Double)row.get("net_sale"));
				
				reportRespList.add(respRow);
			}
			
		}catch(DataAccessException dae){
			throw new DataAccessLayerException(dae.getMessage());
		}
		
		wrapper.setReportRespList(reportRespList);
			
		wrapper.setStatus(true);
		
		return wrapper;
	}
	
	//GET_EXPWISE_REPORT_DATA
	public ReportsResponseWrapper getExpWiseData(String type, Date from, Date to){
		
		ReportsResponseWrapper wrapper = new ReportsResponseWrapper();
		List<ReportRespObject> reportRespList = new ArrayList<>();
		
		if(type != null){
			
			Object[] params = {type, from, to};
			int[] types = {Types.VARCHAR, Types.DATE, Types.DATE};
			try{
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(GET_EXPWISE_REPORT_DATA, params, types);
				for(Map<String, Object> row : rows){
					ReportRespObject respRow = new ReportRespObject();
					respRow.setDate((Date)row.get("expns_date"));
					respRow.setSaleRs((Double)row.get("amount"));
					respRow.setType(row.get("expense_type").toString());
					respRow.setDesc(row.get("description").toString());
					
					reportRespList.add(respRow);
				}
				
			}catch(DataAccessException dae){
				throw new DataAccessLayerException(dae.getMessage());
			}
			
		}else{
			
			Object[] params = {from , to};
			int[] types = {Types.DATE, Types.DATE};
			try{
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(GET_EXP_REPORT_DATA, params, types);
				for(Map<String, Object> row : rows){
					ReportRespObject respRow = new ReportRespObject();
					respRow.setDate((Date)row.get("expns_date"));
					respRow.setSaleRs((Double)row.get("amount"));
					respRow.setId(row.get("expense_id").toString());
					respRow.setType(row.get("expense_type").toString());
					respRow.setDesc(row.get("description").toString());
					
					reportRespList.add(respRow);
				}
				
			}catch(DataAccessException dae){
				throw new DataAccessLayerException(dae.getMessage());
			}
		}
		wrapper.setReportRespList(reportRespList);
		wrapper.setStatus(true);
		
		return wrapper;
	}
	
}
