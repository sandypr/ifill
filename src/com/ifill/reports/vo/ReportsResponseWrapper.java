/**
 * 
 */
package com.ifill.reports.vo;

import java.util.List;
import java.util.Map;

/**
 * @author Sandy
 *
 */
public class ReportsResponseWrapper {

	// status
	private boolean status; 
	// serves for direct key-values
	private Map<String,String> reports;
	// serves for detailed reports
	private List<ReportRespObject> reportRespList;

	public Map<String, String> getReports() {
		return reports;
	}

	public void setReports(Map<String, String> reports) {
		this.reports = reports;
	}
	
	public String getValue(String key){
		if(reports.containsKey(key))
			return reports.get(key);
		else 
			return null;
	}

	public List<ReportRespObject> getReportRespList() {
		return reportRespList;
	}

	public void setReportRespList(List<ReportRespObject> reportRespList) {
		this.reportRespList = reportRespList;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
